using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FloppyEncoder
{
    public partial class Form1 : Form
    {
        byte[] sony_track_len = {
	        12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12,
	        11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
	        10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
	         9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,
	         8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8
        };

        byte[] sony_to_disk_byte = {
	        0x96, 0x97, 0x9A, 0x9B,  0x9D, 0x9E, 0x9F, 0xA6, /* 0x00 */
	        0xA7, 0xAB, 0xAC, 0xAD,  0xAE, 0xAF, 0xB2, 0xB3,
	        0xB4, 0xB5, 0xB6, 0xB7,  0xB9, 0xBA, 0xBB, 0xBC, /* 0x10 */
	        0xBD, 0xBE, 0xBF, 0xCB,  0xCD, 0xCE, 0xCF, 0xD3,
	        0xD6, 0xD7, 0xD9, 0xDA,  0xDB, 0xDC, 0xDD, 0xDE, /* 0x20 */
	        0xDF, 0xE5, 0xE6, 0xE7,  0xE9, 0xEA, 0xEB, 0xEC,
	        0xED, 0xEE, 0xEF, 0xF2,  0xF3, 0xF4, 0xF5, 0xF6, /* 0x30 */
	        0xF7, 0xF9, 0xFA, 0xFB,  0xFC, 0xFD, 0xFE, 0xFF
        };

        // from MESS
        void mess_sony_nibblize35(byte[] dataIn, byte[] nib_ptr, byte[] csum)
        {
            int	i, j;
            UInt32 c1, c2, c3, c4;
            byte val;
            byte w1, w2, w3, w4;
            byte[] b1 = new byte[175];
            byte[] b2 = new byte[175];
            byte[] b3 = new byte[175];

            /* Copy from the user's buffer to our buffer, while computing
             * the three-byte data checksum
             */

            i = 0;
            j = 0;
            c1 = 0;
            c2 = 0;
            c3 = 0;
            while (true)
            {
	            // ROL.B
                      c1 = (c1 & 0xFF) << 1;
	            if ((c1 & 0x0100) != 0)
		            c1++;

	            val = dataIn[i++];
	            // ADDX?
                      c3 += val;
	            if ((c1 & 0x0100) != 0)
	            {
		            c3++;
		            c1 &= 0xFF;
	            }

	            b1[j] = (byte)((val ^ c1) & 0xFF);

	            val = dataIn[i++];
	            c2 += val;
	            if (c3 > 0xFF)
	            {
		            c2++;
		            c3 &= 0xFF;
	            }
	            b2[j] = (byte)((val ^ c3) & 0xFF);

	            if (i == 524) break;

	            val = dataIn[i++];
	            c1 += val;
	            if (c2 > 0xFF)
	            {
		            c1++;
		            c2 &= 0xFF;
	            }
	            b3[j] = (byte)((val ^ c2) & 0xFF);
	            j++;
            }
            c4 =  ((c1 & 0xC0) >> 6) | ((c2 & 0xC0) >> 4) | ((c3 & 0xC0) >> 2);
            b3[174] = 0;

            j = 0;
            for (i = 0; i <= 174; i++)
            {
	            w1 = (byte)(b1[i] & 0x3F);
	            w2 = (byte)(b2[i] & 0x3F);
	            w3 = (byte)(b3[i] & 0x3F);
	            w4 =  (byte)(((b1[i] & 0xC0) >> 2));
	            w4 |= (byte)(((b2[i] & 0xC0) >> 4));
	            w4 |= (byte)(((b3[i] & 0xC0) >> 6));

	            nib_ptr[j++] = w4;
	            nib_ptr[j++] = w1;
	            nib_ptr[j++] = w2;

	            if (i != 174) nib_ptr[j++] = w3;
            }

            csum[0] = (byte)(c1 & 0x3F);
            csum[1] = (byte)(c2 & 0x3F);
            csum[2] = (byte)(c3 & 0x3F);
            csum[3] = (byte)(c4 & 0x3F);
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofg = new OpenFileDialog();
            ofg.Title = "Select the floppy image file ";
            ofg.Filter = "DSK files|*.dsk|All files|*.*";

            if (ofg.ShowDialog() == DialogResult.OK)
            {

                FileStream rfs = new FileStream(ofg.FileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                BinaryReader readFile = new BinaryReader(rfs);

                if (rfs.Length != 819200 && rfs.Length != 409600)
                {
                    MessageBox.Show(String.Format("{0} does not appear to be a 400K or 800K disk image", System.IO.Path.GetFileName(ofg.FileName)), "Error");
                }
                else
                {
                    SaveFileDialog sfg = new SaveFileDialog();
                    sfg.Title = "Save Encoded Floppy As";
                    sfg.Filter = "BIN files|*.bin|All files|*.*";
                    sfg.FileName = System.IO.Path.ChangeExtension(ofg.FileName, ".bin");

                    if (sfg.ShowDialog() == DialogResult.OK)
                    {
                        FileStream wfs = new FileStream(sfg.FileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                        BinaryWriter writeFile = new BinaryWriter(wfs);

                        byte format = 0x22; // 0x22 = MacOS double-sided, 0x02 = single sided
                        byte numSides = (byte)(rfs.Length / 409600);

                        byte side = 0;
                        UInt16 track = 0;
                        byte sectorInTrack = 0;
                        int offset = 0;

                        while (offset < rfs.Length)
                        {
#if false
                            // Pseudo-sync pattern
                            for (int i = 0; i < 14 * 4; i++)
                                writeFile.Write((byte)0xff);
#endif

                            byte trackLow = (byte)(track & 0x3f);
                            byte trackHigh = (byte)((side << 5) | (track >> 6));
                            byte checksum = (byte)((trackLow ^ sectorInTrack ^ trackHigh ^ format) & 0x3F);                  

                            // Address block
                            writeFile.Write((byte)0xd5);
                            writeFile.Write((byte)0xaa);
                            writeFile.Write((byte)0x96);
                            writeFile.Write(sony_to_disk_byte[trackLow]);
                            writeFile.Write(sony_to_disk_byte[sectorInTrack]);
                            writeFile.Write(sony_to_disk_byte[trackHigh]);
                            writeFile.Write(sony_to_disk_byte[format]);
                            writeFile.Write(sony_to_disk_byte[checksum]);
                            writeFile.Write((byte)0xde);
                            writeFile.Write((byte)0xaa);

                            // More syncs
                            for (int i = 0; i < 5; i++)
                                writeFile.Write((byte)0xff); 

                            // Data block
                            writeFile.Write((byte)0xd5);
                            writeFile.Write((byte)0xaa);
                            writeFile.Write((byte)0xad);
                            writeFile.Write(sony_to_disk_byte[sectorInTrack]);

                            int nibCount = 699;
                            byte[] dataIn = new byte[512+12];
                            byte[] nibOut = new byte[nibCount];
                            byte[] dataChecksum = new byte[4];

                            // get the tags and sector data
                            for (int i = 0; i < 12; i++)
                            {
                                dataIn[i] = 0;
                            }
                            for (int i = 0; i < 512; i++)
                            {
                                byte b = readFile.ReadByte();
                                dataIn[i + 12] = b;
                            }

                            // convert the sector data
                            mess_sony_nibblize35(dataIn, nibOut, dataChecksum);

                            // write the sector data and the checksum
                            for (int i = 0; i < nibCount; i++)
                            {
                                writeFile.Write(sony_to_disk_byte[nibOut[i]]);
                            }

                            for (int i = 3; i >= 0; i--)
                            {
                                writeFile.Write(sony_to_disk_byte[dataChecksum[i]]);
                            }

                            // data block trailer
                            writeFile.Write((byte)0xde);
                            writeFile.Write((byte)0xaa);
                            writeFile.Write((byte)0xff);

#if false
                            // padding to make a power of 2 size for encoded sectors
                            for (int i = 0; i < 243; i++)
                            {
                                writeFile.Write((byte)0xff);
                            }
#endif

                            // next sector
                            offset += 512;
                            sectorInTrack++;
                            if (sectorInTrack == sony_track_len[track])
                            {
                                sectorInTrack = 0;

                                if (numSides == 2 && side == 0)
                                {
                                    side = 1;
                                }
                                else
                                {
                                    track++;
                                    side = 0;
                                }
                            }
                        }

                        writeFile.Close();
                        readFile.Close();

                        rfs.Close();
                        wfs.Close();

                        MessageBox.Show("Success!", "Floppy Encoder Result");
                    }

                }

                Close();
            }
        }
    }
}