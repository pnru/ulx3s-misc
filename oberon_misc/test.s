	iobase = -64
	led    = 4
	SioDt  = 8
	SioSt  = 12
	TxRdy  = 2
	RxRdy  = 1
	
	br	start
	
start:	mov	r14,iobase	; set IOBASE and set leds
	mov	r0,0xaa
	st	r0,led(r14)
	
	mov	r1,0		; baud = 19K2
	st	r1,SioSt(r14)
	
	mov	r0,0		; send NUL NUL to sync
	bl	send
	bl	send

	; output increasing numbers in hex
	;
	mov	r10,0		; start at 0
loop:	mov	r0,r10		; send address
	bl	hex8
	mov	r0,0x20		; send space
	bl	send
	ld	r0,0(r10)	; send content
	bl	hex8
	mov	r0,0x0a		; send \n
	bl	send
	add	r10,r10,4	; next number
	br	loop

	; send char in r0 to UART
	;
send:	ld	r13,SioSt(r14)	; wait Tx ready
	and	r13,r13,TxRdy
	beq	send
	st	r0,SioDt(r14)	; send char
	rt

	; receive char from UART to r0
	;
recv:	ld	r13,SioSt(r14)	; wait Rx ready
	and	r13,r13,RxRdy
	beq	recv
	ld	r0,SioDt(r14)	; recv char
	rt

	; send word in r0 in hex
	;
hex8:	mov	r3,r15		; save linkage
	mov	r4,8		; 8 digits
hx01:	ror	r0,r0,28	; next digit
	bl	hex1
	add	r4,r4,-1
	bne	hx01
	br	(r3)		; return

	; send one hex digit
	;
hex1:	mov	r2,r15		; save linkage
	mov	r1,r0		; save r0
	and	r0,r1,0xf	; mask lowest digit in r0
	sub	r12,r0,0xa	; decimal?
	bmi	hex1a		; -> yes
	add	r0,r0,0x27	; add 'a' - '0' - 10
hex1a:	add	r0,r0,0x30	; add '0'
	bl	send		; send ascii digit
	mov	r0,r1		; restore r0
	br	(r2)		; return

