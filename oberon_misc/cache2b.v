/*
 * Simple set-associative cache
 *
 * This source code is public domain
 *
 */

module CACHE (

  // CPU interface
  input  wire        clk,         // CPU clk
  input  wire        rst,         // reset

  input  wire [19:0] addr,        // address (1MB range)
  input  wire [31:0] din,         // data to cache
  output wire [31:0] dout,        // data to CPU
  input  wire  [3:0] wmask,       // byte write mask
  input  wire        mreq,        // request mem transaction
  output reg         mrdy,        // report mem ready

  // SDRAM interface
  input  wire        sdr_clk,     // SDR clk
  output reg  [11:0] sdr_addr,    // high part of address (256 byte line)
  input  wire [15:0] sdr_din,     // data to cache
  output wire [15:0] sdr_dout,    // data to SDR
  input  wire        sdr_get,     // load word from SDR on sdr_clk
  input  wire        sdr_put,     // send word to SDR on sdr_clk
  output reg         sdr_rd = 0,  // SDR start read transaction
  output reg         sdr_wr = 0   // SDR start write transaction
);

  localparam bWAY = 2, WAYS = (1<<bWAY);  // 2 bits,  4 way
  localparam bSET = 4, SETS = (1<<bSET);  // 4 bits, 16 sets

  wire req = mreq; //(mreq===1'bx) ? 1'b0 : mreq; // for RISC5 + iverilog only

  // Example split up of 20 bit address range for bSET = 4:
  // { [19:12] tag ; [11:8] set ; [7:0] (byte) cache line }
  //
  wire  [bSET-1:0] set = addr[bSET+7:8];
  wire [11-bSET:0] tag = addr[19:bSET+8];

  // For each way in the cache define a tag table & comparator
  //
  reg  [11-bSET:0] tag0[0:SETS-1], tag1[0:SETS-1], tag2[0:SETS-1], tag3[0:SETS-1], wtag, rtag;

  wire fnd0 = (tag0[set] == tag), fnd1 = (tag1[set] == tag);
  wire fnd2 = (tag2[set] == tag), fnd3 = (tag3[set] == tag);
  wire hit  = fnd0 | fnd1 | fnd2 | fnd3;

  wire [bWAY-1:0] cway = { fnd3|fnd2, fnd3|fnd1 };
  reg  [bWAY-1:0] sway;
  reg  [bSET-1:0] sset;
  reg             dirty;
  
  always @(negedge clk) begin
    if (req & !hit & (repl==0)) begin wtag <= tag0[set]; tag0[set] <= tag; dirty <= dirty0[set]; end
    if (req & !hit & (repl==1)) begin wtag <= tag1[set]; tag1[set] <= tag; dirty <= dirty1[set]; end
    if (req & !hit & (repl==2)) begin wtag <= tag2[set]; tag2[set] <= tag; dirty <= dirty2[set]; end
    if (req & !hit & (repl==3)) begin wtag <= tag3[set]; tag3[set] <= tag; dirty <= dirty3[set]; end
    if (mrdy) begin sset <= set; rtag <= tag; sway <= cway; end
    mrdy <= stby & (!req || hit);
  end
  
  // Handle dirty bits
  //
  reg   [SETS-1:0] dirty0, dirty1, dirty2, dirty3;
  wire  wr = |wmask;
  
  always @(posedge clk) begin
    if (fnd0) dirty0[sset] <= (dirty0[sset] & mrdy) | wr;
    if (fnd1) dirty1[sset] <= (dirty1[sset] & mrdy) | wr;
    if (fnd2) dirty2[sset] <= (dirty2[sset] & mrdy) | wr;
    if (fnd3) dirty3[sset] <= (dirty3[sset] & mrdy) | wr;
  end

  // Replacement policy: "Random"
  //
  reg [bWAY-1:0] repl = 0;
  always @(posedge clk) repl <= repl + 1;
  
  // Cache-SDRAM controller
  //
  localparam STBY = 4'd0, RD = 4'd1, RDWT = 4'd2, WR = 4'd3, WRWT = 4'd4, WT = 4'd5;

  reg  [3:0] STATE = 0;

  wire stby    = (STATE == STBY) | (STATE == RDWT);
  wire zctr    = (STATE == RDWT) | (STATE == WRWT);
  wire done;

  always @(posedge sdr_clk) begin
    sdr_rd <= (STATE==RD);
    sdr_wr <= (STATE==WR);
  end

  reg [5:0] page;
  
  always @(posedge sdr_clk) begin
    if (rst) begin STATE <= 0; end
    case (STATE)

    STBY: if (!mrdy) STATE <= WT;
    
    WT:   begin
            sdr_addr <= dirty ? { wtag, sset } : { rtag, sset };
            STATE    <= dirty ?      WR       :      RD;
          end
            
    // Write cache line
    WR:   if (done) begin
            sdr_addr <= { rtag, sset };
            STATE <= WRWT;
          end
    WRWT: STATE <= RD;

    // Read cache line
    RD:   if (done) begin
            STATE <= RDWT;
          end
    RDWT: if (!req) STATE <= STBY;

    endcase
    end
  end

  // Mirror SDRAM address: count out 256 bytes (128 sdram words)
  reg [7:0] sdr_ctr;
  
  always @(posedge sdr_clk) begin
    if (sdr_get | sdr_put) sdr_ctr <= sdr_ctr + 1;
    else if (zctr|rst) sdr_ctr <= 8'b0;
  end
  assign done = sdr_ctr[7];  // full cache line processed

  wire cpu_get = mrdy & ~clk & req & hit & wr;
  
  // The actual cache data store, 32b x 4K single port RAM, with write-enable per
  // byte. It is connected to the CPU when mrdy==1 and to the SDRAM otherwise.
  // For the SDRAM side, convert between 16b x 8K <=> 32b x 4K
  //
  wire [bWAY+bSET+5:0] caddr = mrdy ? { cway, set   ,  addr[7:2] }   // CPU side address
                                    : { sway, sset, sdr_ctr[6:1] };  // SDRAM side address
  
  wire          [31:0]  cdin = mrdy ? din
                                    : { sdr_din, sdr_din };          // write selection via cwe
  
  wire           [3:0]   cwe = mrdy ? {4{cpu_get}} & wmask
                                    : {4{sdr_get}} & (sdr_ctr[0] ? 4'b1100 : 4'b0011);

  reg wsel;
  always @(negedge sdr_clk) wsel <= sdr_ctr[0];
  assign sdr_dout = wsel ? dout[15:0] : dout[31:16];
  
  wire [3:0] tst;
  
  CRAM cram (
    .c_clk(sdr_clk),
    .c_addr(caddr),
    .c_din(cdin),
    .c_dout(dout),
    .c_wmask(cwe)
  );

  // init tags to be non-conflicting
  //
  integer j;
  initial begin
      for(j=0; j<SETS; j=j+1) begin
        tag0[j]   = 0;      tag1[j] = 1;      tag2[j] = 2;      tag3[j] = 3;
        dirty0[j] = 1'b0; dirty1[j] = 1'b0; dirty2[j] = 1'b0; dirty3[j] = 1'b0;
      end
  end

endmodule

module CRAM (
  input  wire        c_clk,         // SDRAM clock
  input  wire [11:0] c_addr,        // address
  input  wire [31:0] c_din,         // data to mem
  output reg  [31:0] c_dout,        // data from mem
  input  wire  [3:0] c_wmask        // byte write mask
);

  reg [31:0] mem[0:4095];

  always @(posedge c_clk) begin
    c_dout <= mem[c_addr];
  end
  
  always @(posedge c_clk) begin
    if (c_wmask[3]) mem[c_addr][31:24] <= c_din[31:24];
    if (c_wmask[2]) mem[c_addr][23:16] <= c_din[23:16];
    if (c_wmask[1]) mem[c_addr][15: 8] <= c_din[15: 8];
    if (c_wmask[0]) mem[c_addr][ 7: 0] <= c_din[ 7: 0];
  end
    
endmodule