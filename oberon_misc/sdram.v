//
// sdram.v
//
// This source code is public domain
//
// Oberon SDRAM controller, do 256 byte bursts
//   PC133 (grade 7) chips work up to 143Mhz
//   PC166 (grade 6) chips work up to 166MHz
//

module SDRAM (
  input             clk_in,     // controller clock
  
  // interface to the chip
  inout      [15:0] sd_data,    // 16 bit databus
  output reg [12:0] sd_addr,    // 12 bit multiplexed address bus
  output reg [1:0]  sd_dqm,     // two byte masks
  output reg [1:0]  sd_ba,      // two banks
  output            sd_cs,      // chip select
  output            sd_we,      // write enable
  output            sd_ras,     // row address select
  output            sd_cas,     // columns address select
  output            sd_cke,     // clock enable
  output            sd_clk,     // chip clock (inverted from input clk)

  // Oberon cache interface
  input      [15:0] din,        // data input from cpu
  output reg [15:0] dout,       // data output to cpu
  input      [11:0] ad,         // 12 bit upper address
  output wire       sd_clk2,    // load clock
  output reg        get = 0,    // load word from SDR on sdr_clk2
  output reg        put = 0,    // send word to SDR on sdr_clk2
  input  wire       rd,         // SDR start read transaction
  input  wire       wr,         // SDR start write transaction
  input             rst         // cpu reset
);

  localparam RASCAS_DELAY   = 3'd3;   // tRCD=20ns -> 3 cycles @ >100MHz
  localparam BURST_LENGTH   = 3'b111; // 000=1, 001=2, 010=4, 011=8, 111=full page
  localparam ACCESS_TYPE    = 1'b0;   // 0=sequential, 1=interleaved
  localparam CAS_LATENCY    = 3'd3;   // 3 needed @ >100Mhz
  localparam OP_MODE        = 2'b00;  // only 00 (standard operation) allowed
  localparam NO_WRITE_BURST = 1'b0;   // 0=write burst enabled, 1=only single access write

  localparam MODE = { 3'b000, NO_WRITE_BURST, OP_MODE, CAS_LATENCY, ACCESS_TYPE, BURST_LENGTH};
  
  // Sync & extend the address bus
  //reg [23:0] addr;
  //always @(posedge clk_in) addr <= { 4'h0, ad, 8'h00 };
  wire [23:0] addr;
  assign addr = { 5'h0, ad, 7'h0 };


  // ---------------------------------------------------------------------
  // --------------------------- startup/reset ---------------------------
  // ---------------------------------------------------------------------

  // make sure rst lasts long enough (recommended 100us)
  reg [4:0] reset;
  always @(posedge clk_in) begin
    reset <= (|reset) ? reset - 5'd1 : 0;
    if(rst)	reset <= 5'd25;
  end

  // ---------------------------------------------------------------------
  // ------------------ generate ram control signals ---------------------
  // ---------------------------------------------------------------------

  // all possible commands
  localparam CMD_INHIBIT         = 4'b1111;
  localparam CMD_NOP             = 4'b0111;
  localparam CMD_ACTIVE          = 4'b0011;
  localparam CMD_READ            = 4'b0101;
  localparam CMD_WRITE           = 4'b0100;
  localparam CMD_BURST_TERMINATE = 4'b0110;
  localparam CMD_PRECHARGE       = 4'b0010;
  localparam CMD_AUTO_REFRESH    = 4'b0001;
  localparam CMD_LOAD_MODE       = 4'b0000;

  reg  [3:0] sd_cmd = CMD_INHIBIT; // current command sent to sd ram

  assign sd_clk = !clk_in; // chip clock shifted 180 deg.
  assign sd_cke = 1'b1;

  // drive control signals according to current command
  assign sd_cs  = sd_cmd[3];
  assign sd_ras = sd_cmd[2];
  assign sd_cas = sd_cmd[1];
  assign sd_we  = sd_cmd[0];

  // sdram tri-state databus interaction
  reg  sd_data_wr = 1'b0;
  reg  sd_data_rd = 1'b0;
  assign sd_data  = sd_data_wr ? din : 16'hzzzz;
`ifdef __ICARUS__
  always @(posedge sd_clk) if(sd_data_rd) dout <= sd_data;
  assign sd_clk2 = sd_clk;
`else
  wire rd_clk;
  DLY2NS dly(.in(sd_clk), .out(rd_clk));
  IFS1P3BX dbi_FF[15:0] (.SCLK(rd_clk), .SP(sd_data_rd), .Q(dout), .D(sd_data), .PD(1'b0));
  assign sd_clk2 = rd_clk;
`endif
  
  // ---------------------------------------------------------------------
  // ------------------------ cycle state machine ------------------------
  // ---------------------------------------------------------------------
  
  // The state machine runs at 125Mhz, asynchronous to the CPU.
  // It idles doing refreshes and switches to burst reads and writes
  // as requested.
  
  localparam STBY  =   0;   // start state, do refreshes
  localparam RD    =  50;   // start of read sequence
  localparam WR    = 200;   // start of write sequence

  reg [8:0] t = 0;

  wire mreq =  rd | wr;

  always @(posedge clk_in) begin
    sd_cmd <= CMD_NOP;  // default: NOP
    
    // move to next state
    t <= t + 1;

    if(reset != 0) begin // reset operation
      case(reset)
      22: sd_ba   <= 2'b00;
      21: sd_addr[10] <= 1'b1; // prep for precharge all banks
      20: sd_cmd  <= CMD_PRECHARGE;
      11: sd_addr <= MODE;
      10: sd_cmd  <= CMD_LOAD_MODE;
       1: t <= 0;
      endcase
    end
    
    else begin // normal operation
      case(t)

      STBY:  begin
                sd_addr <= { 2'b00, addr[19:9] };
                sd_ba   <= addr[21:20];
                if (mreq) begin
                  sd_cmd <= CMD_ACTIVE;
                  t <= rd ? RD : WR;
                end
                else begin
                  sd_cmd <= CMD_AUTO_REFRESH;
                end
              end
      STBY+9: t <= 0;
      
      // Read burst, set-up for RCD = 2 clocks and CL = 3 clocks. Possibly the burst
      // terminate could come earlier. The 'get' signal accounts for the dbi_FF delay.
      //
      RD+2:   begin sd_dqm     <= 2'b00;  sd_addr <= { 4'b0010, addr[8:0] }; end
      RD+3:   begin sd_cmd     <= CMD_READ;                                  end
      RD+6:   begin sd_data_rd <= 1'b1;                                      end
      RD+7:   begin get        <= 1'b1;                                      end
      // ... 128 read cycles
      RD+134: begin sd_data_rd <= 1'b0;                                      end
      RD+135: begin get        <= 1'b0;                                      end
      RD+136: begin sd_cmd     <= CMD_BURST_TERMINATE;                       end
      RD+137: begin sd_cmd     <= CMD_PRECHARGE; /* ALL */                   end
      RD+140: t <= 0;
      
      // Write burst, set up fro RCD = 2. Burst terminate has to be exact here, and
      // the 'put' signal accounts for 1 clock delay to fetch data.
      //
      WR+2:   begin sd_dqm     <= 2'b00; sd_addr <= { 4'b0010, addr[8:0] };\
                    sd_data_wr <= 1'b1;  put        <= 1'b1;                 end
      WR+3:   begin sd_cmd    <= CMD_WRITE;                                  end
      // ... 128 write cycles
      WR+130: begin put        <= 1'b0;                                      end
      WR+131: begin sd_cmd     <= CMD_BURST_TERMINATE; sd_data_wr <= 1'b0;   end
      WR+132: begin sd_cmd     <= CMD_PRECHARGE; /* ALL */                   end  
      WR+140: t <= 0;

      endcase
    end
  end

endmodule

// poor solution to generate ~2ns delay
//
`ifndef __ICARUS__

module DLY2NS (
  input  wire in,
  output wire out
);

  (* keep *) wire x1 = !in;
  (* keep *) wire x2 = !x1;
  (* keep *) wire x3 = !x2;
  (* keep *) wire x4 = !x3;
  assign out = x4;
  
endmodule

`endif
