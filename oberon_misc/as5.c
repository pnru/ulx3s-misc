#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

struct opr {
  char *mne;
  int   type;
  int   code;
};

#define FX 9
#define F0 0
#define F1 1
#define F2 2
#define F3 3
#define F4 4

struct opr ins[] = {
 { "adc",  FX, 0x20080000 },
 { "add",  FX, 0x00080000 },
 { "and",  FX, 0x00040000 },
 { "ann",  FX, 0x00050000 },
 { "asr",  FX, 0x00020000 },
 { "bcc",  F3, 0xca000000 },
 { "bcs",  F3, 0xc2000000 },
 { "beq",  F3, 0xc1000000 },
 { "bge",  F3, 0xcd000000 },
 { "bgt",  F3, 0xce000000 },
 { "bhi",  F3, 0xcc000000 },
 { "ble",  F3, 0xc6000000 },
 { "bls",  F3, 0xc4000000 },
 { "blt",  F3, 0xc5000000 },
 { "bmi",  F3, 0xc0000000 },
 { "bne",  F3, 0xc9000000 },
 { "bpl",  F3, 0xc8000000 },
 { "br",   F3, 0xc7000000 },
 { "bl",   F3, 0xd7000000 },
 { "bvc",  F3, 0xcb000000 },
 { "bvs",  F3, 0xc3000000 },
 { "div",  FX, 0x000b0000 },
 { "fad",  FX, 0x000c0000 },
 { "fdv",  FX, 0x000f0000 },
 { "fml",  FX, 0x000e0000 },
 { "fsb",  FX, 0x000d0000 },
 { "ior",  FX, 0x00060000 },
 { "ld",   F2, 0x80000000 },
 { "ldb",  F2, 0x90000000 },
 { "ldfl", F0, 0x30000000 },
 { "ldhi", F1, 0x60000000 },
 { "ldmq", F0, 0x20000000 },
 { "lsr",  FX, 0x00010000 },
 { "mov",  FX, 0x00000000 },
 { "muls", FX, 0x000a0000 },
 { "mulu", FX, 0x200a0000 },
 { "nop",  F4, 0xcf000000 },
 { "ror",  FX, 0x00030000 },
 { "rt",   F4, 0xc700000f },
 { "sbc",  FX, 0x20090000 },
 { "st",   F2, 0xa0000000 },
 { "stb",  F2, 0xb0000000 }, 
 { "sub",  FX, 0x00090000 },
 { "xor",  FX, 0x00070000 },
 { NULL,   0,  0 }
};
typedef struct opr Opr;

// Globals
//
int      num;
int      sep;
char     str[16];
int      PC;
int      pass;

// symbol table
//
struct Sym {
  char  name[16];
  int   val;
  int   flg;
};
typedef struct Sym Sym;

Sym syms[100];
int nsym;
Sym*     sym;

void
enter(int val)
{
  if (nsym==100) {
    printf("Symbol table overflow\n");
    exit(1);
  }
  sym = &syms[nsym];
  strcpy(sym->name, str);
  sym->val = val;
  nsym++;
}

Sym *
search()
{
  int i;

  for(i=0; i<nsym; i++) {
     if (strcmp(str, syms[i].name)==0) break;
  }
  return (i==nsym) ? NULL : &syms[i];
}

void
fndlab()
{
  sym = search();
  if (sym==NULL) {
    if (pass==2) {
      printf("Undefined label\n");
      exit(1);
    }
    else
      enter(0);
  }
  num = sym->val;
}

void
setlab(int val)
{
  sym = search();
  if (pass==1) {
    if (sym && sym->flg) {
      printf("Duplicate label definition\n");
      exit(1);
    }
    if (!sym) {
      enter(val);
    }
    sym->val = val;
    sym->flg = 1;
  }
}

void
prtsyms()
{
  int i;
  
  printf("\nSYMBOLS:\n");
  for(i=0; i<nsym; i++) {
    printf("%-20s %08x\n", syms[i].name, syms[i].val);
  }
  printf("\n");
}

// tokenizer
//
char    *line, *p;
ssize_t  n;

#define NUM 1
#define STR 2
#define SEP 3

int
tok()
{
  while (isspace(*p)) p++;

  if (*p==0 || p==line+n) {
    return -1;
  }
  else if (isdigit(*p) || *p=='-' || *p=='+') {
    /* scan number */
    num = strtoul(p, &p, 0);
    return NUM;
  }
  else if (isalpha(*p)) {
    /* scan name */
    char *q;
    sym = NULL;
    q = &str[0];
    while (isalnum(*p) && q!=&str[15]) {
      *q++ = *p++;
    }
    *q = 0;
    return STR;
  }
  else if (*p=='\'') {
    /* character constant */
    p++; num = *p++;
    if (num=='\\') num = *p++;
    if (*p++!='\'') {
      printf("Unclosed character constant\n");
      exit(1);
    }
    return NUM;
  }
  else {
    /* separator char */
    sep = *p++;
    return SEP;
  }
}

Opr *
isopr()
{
  Opr *idx = ins;
  
  while (idx->mne) {
    if (strcmp(str,idx->mne)==0) break;
    idx++;
  }
  return (idx->mne) ? idx : NULL;
}

int
isreg()
{
  char *q = &str[0];
  int r;
  
  if (*q!='r') return -1;
  q++;
  if (!isdigit(*q)) return -1;
  r = *q - '0';
  q++;
  if (!isdigit(*q)) return r;
  r = r * 10 + *q - '0';
  return r;
}

// Parse instruction types
//

int
indir()
{
  int t,r;

  t = tok();
  if (t!=STR || (r = isreg())<0) {
    printf("Syntax error, '(rx)' expected\n");
    exit(1);
  }
  t = tok();
  if (t!=SEP || sep!=')') {
    printf("Syntax error, '(rx)' expected");
    exit(1);
  }
  return r;
}

int
getreg(int t, int comma)
{
  int r;

  if (t!=STR || (r = isreg())<0) {
    printf("Syntax error, 'rx' expected\n");
    exit(1);
  }
  if (comma) {
    t = tok();
    if (t!=SEP || sep!=',') {
      printf("Syntax error, ',' expected\n");
      exit(1);
    }
  }
  return r;
}

FILE    *fq;

void
outcode(int code)
{
  if (pass==2) {
    printf("%04x  ", PC);
    printf("%08x  ", code);
    fprintf(fq, "%08x\n", code);
  }
  PC += 4;
}

void
do_F0(Opr* opr)
{
  int t,a=0,code;

  t = tok(); a = getreg(t, 0);
  code = opr->code | a <<24;
  outcode(code);
}

void
do_FX(Opr* opr)
{
  int t,r,imm=0,a=0,b=0,c=0,code;
  int mov;
  
  mov = (opr->code&0x000f0000)==0;
  t = tok(); a = getreg(t, 1);
  if (!mov) {
    t = tok(); b = getreg(t, 1);
  }
  code = opr->code | a <<24 | b<<20;
  t = tok();
  if (t==NUM) {
    imm = num;
    code |= 0x40000000;
  }
  else if (t==STR) {
    if ((c = isreg())>=0) {
      code |= c;
    }
    else {     
      fndlab();
      imm = sym->val;
      code |= 0x40000000;
    }
  }
  b = imm>>16;
  if (mov && b!=0 && b!=-1) {
    b &= 0xffff;
    outcode(code | 0x20000000 | b);  // ldhi rx,upr
    if (pass==2) printf("|\n");
    code |= 0x00060000;              // ior  rx,lwr
    imm &= 0xffff;
  }
  if (imm<0)
    code |= 0x10000000;
  code |= imm & 0xffff;
  outcode(code);
}

void
do_F2(Opr* opr)
{
  int t,offs=0,a=0,b=0,code;

  t = tok(); a = getreg(t, 1);
  t = tok();
  if (t==NUM) {
    offs = num;
  }
  else if (t==STR) {
    fndlab();
    offs = sym->val;
  }
  t = tok();
  if (t==SEP && sep=='(') {  
    b = indir();
  }
  code = opr->code | a <<24 | b<<20 | (offs & 0x000fffff);
  outcode(code);
}

void
do_F3(Opr* opr)
{
  int t,r,offs;

  t = tok();
  if (t==SEP && sep=='(') {
    r = indir();
    outcode(opr->code | r);
  }
  else if (t==STR) {
    fndlab();
    offs = (num - PC - 4) >> 2;
    outcode(opr->code | 0x20000000 | (offs&0x00ffffff));
  }
  else {
    printf("Syntax error, label or '(rx)' expected");
    exit(1);
  }
}

void
do_F4(Opr* opr)
{
  outcode(opr->code);
}

void
do_line()
{
  int      t, i, c, e = 0;

  p = line;

  while( (t = tok()) > 0 ) {
    switch(t) {
      case NUM: {
        outcode(num);
        break;
      }
      case STR: {
        Opr *opr = isopr();
        int R    = isreg();
    
        if (opr) {
          switch(opr->type) {
            case FX: do_FX(opr); break;
            case F0: do_F0(opr); break;
            case F2: do_F2(opr); break;
            case F3: do_F3(opr); break;
            case F4: do_F4(opr); break;
          }
          break;
        }
        else if (R>=0) {
          printf("Unexpected register r%d\n", R);
        }
        else {
          t = tok();
          if (t!=SEP || (sep!=':' && sep!='=')) {
            printf("Error in label definition\n");
            exit(1);
          }
          num = PC;
          if (sep=='=') {
            t = tok();
            if (t!=NUM) {
              printf("Error in label definition\n");
              exit(1);
            }
            if (pass==2) printf("        %08x  ", num);
          }
          setlab(num);
        }
        break;
      }
      case SEP: {
        if (sep==';') {
          if (e==0 && pass==2) printf("                  ");
          return;
        }
        if (sep=='"') {
          i = 0; t = 0;
          while (*p && (c = *p++)!='"') {
            t |= (c << 8*i);
            i = (i + 1) & 3;
            if (i==0) {
              outcode(t); if (pass==2) printf("|\n");
              t = 0;
            }
          }
          if (i==0) outcode(0); else outcode(t);
          break;
        }
        printf("Unexpected character 0x%02x\n", sep);
        exit(1);
      }
      e++;
    }
  }
}

#define ORG 0xffe000

int
main(int argc, char **argv)
{
  FILE    *fp;
  size_t   len = 0;

  if (argc<2) {
    printf("usage: %s file\n", argv[0]);
    exit(1);
  }
  fp = fopen(argv[1], "r");
  if (fp == NULL) {
    printf("cannot open: %s\n", argv[1]);
    exit(1);
  }
  fq = fopen("test.mem", "w");
  if (fq == NULL) {
    printf("cannot open: test.mem\n");
    exit(1);
  }

  for(pass=1; pass<=2; pass++) {
    PC = ORG;
    while ((n = getline(&line, &len, fp)) != -1) {
      do_line();
      if (pass==2) printf("%s", line);
    }
    if (pass==1) {
      fseek(fp, 0L, SEEK_SET);
      PC = 0;
    }
    if (pass==2) prtsyms();
  }
  free(line);
  for(; PC<2048; PC+=4) {
    fprintf(fq,"00000000\n");
  }
  fclose(fp); fclose(fq);
  exit(0);
}
