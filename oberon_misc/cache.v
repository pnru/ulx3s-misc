/*
 * Simple set-associative cache
 *
 * This source code is public domain
 *
 */

module CACHE (

  // CPU interface
  input  wire        clk,         // CPU clk
  input  wire        rst,         // reset

  input  wire [19:0] addr,        // address (1MB range)
  input  wire [31:0] din,         // data to cache
  output wire [31:0] dout,        // data to CPU
  input  wire  [3:0] wmask,       // byte write mask
  input  wire        mreq,        // request mem transaction
  output reg         mrdy,        // report mem ready

  // SDRAM interface
  input  wire        sdr_clk,     // SDR clk
  output reg  [11:0] sdr_addr,    // high part of address (256 byte line)
  input  wire [15:0] sdr_din,     // data to cache
  output reg  [15:0] sdr_dout,    // data to SDR
  input  wire        sdr_get,     // load word from SDR on sdr_clk
  input  wire        sdr_put,     // send word to SDR on sdr_clk
  output reg         sdr_rd = 0,  // SDR start read transaction
  output reg         sdr_wr = 0,  // SDR start write transaction

  input  wire flush               // force flush cache
);

  localparam bWAY = 2, WAYS = (1<<bWAY);  // 2 bits,  4 way
  localparam bSET = 4, SETS = (1<<bSET);  // 4 bits, 16 sets

  wire nmrq = (mreq===1'bx) ? 1'b0 : mreq; // for iverilog only

  // break logic loop in RISC5
  always @(negedge clk) mrdy <= stby & (!nmrq || hit);

  wire wr = |wmask;

  // Example split up of 20 bit address range for bSET = 4:
  // { [19:12] tag ; [11:8] set ; [7:0] (byte) cache line }
  //
  wire  [bSET-1:0] set = addr[bSET+7:8];
  wire [11-bSET:0] tag = addr[19:bSET+8];

  // For each way in the cache generate a tag table & comparator
  //
  reg  [11-bSET:0] tags[0:WAYS-1][0:SETS-1];
  reg   [SETS-1:0] dirty[0:WAYS-1];
  wire  [WAYS-1:0] fnd;

  genvar w;
  for(w=0; w<WAYS; w=w+1) begin
    assign fnd[w] = (tags[w][set] == tag);
  end
  wire hit = |fnd;  // if found in any way, it is a hit

  // Calc which 'way' found a match (if any). Only works for bWAY==2!
  //
  wire [bWAY-1:0] way   = { |fnd[3:2], fnd[3]|fnd[1] };

  // Replacement policy: "Random"
  //
  reg [bWAY-1:0] repl = 2;
  always @(posedge clk) repl <= repl + 1;
  
  // Cache-SDRAM controller
  //
  localparam STBY = 4'd0, RD = 4'd1, RDWT = 4'd2, WR = 4'd3, WRWT = 4'd4;

  reg  [3:0] STATE = 0;

  wire stby    = (STATE == STBY);
  wire zctr    = (STATE == RDWT) | (STATE == WRWT);

  wire isdirty = dirty[repl][set];
  wire done;

  always @(posedge clk) begin
    sdr_rd <= (STATE==RD);
    sdr_wr <= (STATE==WR);
  end

  always @(negedge clk) begin
    case (STATE)
    STBY: if (mreq & !rst) begin
            if (hit) begin
              dirty[way][set] <= dirty[way][set] | wr;
            end
            else begin
              tags[repl][set]  <= tag;  // set new tag
              dirty[repl][set] <= wr;   // wr -> start dirty

              if (isdirty)
                sdr_addr <= { tags[repl][set], set };
              else
                sdr_addr <= { tag, set };
              STATE  <= (isdirty) ? WR : RD;
            end
          end
            
    // Write cache line
    WR:   if (done) begin  // wait for xfer to complete
            sdr_addr <= { tag, set };
            STATE <= WRWT;
          end
    WRWT: STATE <= RD; // reset ctr

    // Read cache line
    RD:   if (done) STATE <= RDWT;

    RDWT: STATE <= STBY; // reset ctr
    endcase
  end

  // Mirror SDRAM address: count out 256 bytes (128 sdram words)
  reg [7:0] sdr_ctr;
  
  always @(posedge sdr_clk) begin
    if (sdr_get | sdr_put) sdr_ctr <= sdr_ctr + 1;
    else if (zctr|rst) sdr_ctr <= 8'b0;
  end
  assign done = sdr_ctr[7];  // full cache line processed

  wire cpu_get = stby & ~clk & mreq & hit & wr;
  
  // The actual cache data store, 32b x 4K single port RAM, with write-enable per
  // byte. It is connected to the CPU when mrdy==1 and to the SDRAM otherwise.
  // For the SDRAM side, convert between 16b x 8K <=> 32b x 4K
  //
  wire [bWAY+bSET+5:0] caddr = mrdy ? { way, set,    addr[7:2] }   // CPU side address
                                    : { way, set, sdr_ctr[6:1] };  // SDRAM side address
  
  wire          [31:0]  cdin = mrdy ? din
                                    : { sdr_din, sdr_din };        // write selection via cwe
  
  wire           [3:0]   cwe = mrdy ? {4{cpu_get}} & wmask
                                    : {4{sdr_get}} & (sdr_ctr[0] ? 4'b1100 : 4'b0011);

  always @(negedge sdr_clk) sdr_dout <= sdr_ctr[0] ? dout[15:0] : dout[31:16];
  
  CRAM cram (
    .c_clk(sdr_clk),
    .c_addr(caddr),
    .c_din(cdin),
    .c_dout(dout),
    .c_wmask(cwe)
  );

  // init tags to be non-conflicting
  //
  integer i,j;
  initial begin
    for(i=0; i<WAYS; i=i+1)
      for(j=0; j<SETS; j=j+1) begin
        tags[i][j]  = i;
        dirty[i][j] = 1'b0;
      end
  end

endmodule


module CRAM (

  input  wire        c_clk,         // SDRAM clock
  input  wire [11:0] c_addr,        // address
  input  wire [31:0] c_din,         // data to mem
  output reg  [31:0] c_dout,        // data from mem
  input  wire  [3:0] c_wmask        // byte write mask
);

  reg [31:0] mem[0:4095];
  wire [15:0] ad = { c_addr, 2'b00 };

  always @(posedge c_clk) begin
    c_dout <= mem[c_addr];
  end
  
  always @(posedge c_clk) begin
    if (c_wmask[3]) mem[c_addr][31:24] <= c_din[31:24];
    if (c_wmask[2]) mem[c_addr][23:16] <= c_din[23:16];
    if (c_wmask[1]) mem[c_addr][15: 8] <= c_din[15: 8];
    if (c_wmask[0]) mem[c_addr][ 7: 0] <= c_din[ 7: 0];
  end

  integer i;
  initial begin
    for(i=0; i<4096; i=i+1)
      mem[i] = i<<2;
  end
    
endmodule
