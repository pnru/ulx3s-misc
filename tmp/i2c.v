// Retro I2C access module
//
// This source code is public domain
//

module I2C (
  input  wire        clk,

  input  wire        rs,
  input  wire [15:0] di,
  output reg  [15:0] do,
  input  wire        csn,
  input  wire        rdn,
  input  wire        wrn,
  input  wire        rst,
  
  inout  wire        scl,
  inout  wire        sda
);

  // split open-drain lines into separate i/o signals
  assign sda = sda_e ? sda_o : 1'bZ;
  wire   sda_i = sda;
  
  assign scl = scl_o;
  wire   scl_i = scl;
  
  // derive a 400KHz clock for 100KHz SCL signal
  reg [5:0] clkdiv = 0;
  always @(posedge clk) begin
    clkdiv <= clkdiv + 1;
    if (clkdiv==63) clkdiv <= 0;
  end
  wire tick = !(|clkdiv); // tick = step wire engine
  
  // ---------------------------------------------------------------------
  // ------------------------ I2C bus wire engine ------------------------
  // ---------------------------------------------------------------------

  // SDA out has 4 'values'
  localparam nul = 2'b00, // output 0
             set = 2'b01, // output 1
             bit = 2'b10, // read or write data bit
             ack = 2'b11; // read or write ack bit

  wire sda_e = !(((sda_d==ack) & !sda_m) | ((sda_d==bit) & sda_m));
  wire sda_o = sda_d[1] ? (sda_d[0] ? acko : dato) : sda_d[0];
  
  reg sda_m, i2c_dir = 1;
  always @(posedge clk) if (sync) sda_m <= i2c_dir;       // sync direction signal to cell edge

  reg [1:0] sda_d = set;         // SDA driver state
  reg       scl_o = 1;           // SCL state
  reg       acko  = 0, acki = 0; // output & sampled acknowledge bit

  // State engine for I2C wire signals
  //
  localparam [5:0] IDLE = 0, PURG = 4, XFER = 8, ACKN = 12, STOP = 16, STRT = 20;

  reg [4:0] bus, t = IDLE; // wire state & state counter, runs at 4x SCL frequency
  reg [2:0] bctr = 3'd0;   // bit counter

  always @(posedge clk)
  begin
    if (rst) begin
      t <= 0; bctr <= 0;
    end
    else if (tick) begin
      t <= t + 1;
      case (t)
      // idle: release scl and sda
      IDLE: begin sda_d <= set; scl_o <= 1'b1; end
         1: begin sda_d <= set; scl_o <= 1'b1; end
         2: begin sda_d <= set; scl_o <= 1'b1; end
         3: begin sda_d <= set; scl_o <= 1'b1; end
        // purge: 8 clocks to clear bus
      PURG: begin sda_d <= set; scl_o <= 1'b0; bctr <= bctr + 1; end
         5: begin sda_d <= set; scl_o <= 1'b0; end
         6: begin sda_d <= set; scl_o <= 1'b1; end
         7: begin sda_d <= set; scl_o <= 1'b1; if (bctr!=3'd0) t <= 4; end
        // transfer 8 data bits
      XFER: begin sda_d <= bit; scl_o <= 1'b0; bctr <= bctr + 1; end  
         9: begin sda_d <= bit; scl_o <= 1'b1; end
        10: begin sda_d <= bit; scl_o <= 1'b1; end    
        11: begin sda_d <= bit; scl_o <= 1'b0; if (bctr!=3'd0) t <=  8; end 
        // transfer ACK/NACK
      ACKN: begin sda_d <= ack; scl_o <= 1'b0; end 
        13: begin sda_d <= ack; scl_o <= 1'b1; acki <= sda_i & !i2c_dir; end
        14: begin sda_d <= ack; scl_o <= 1'b1; end 
        15: begin sda_d <= ack; scl_o <= 1'b0; end
        // stop condition
      STOP: begin sda_d <= nul; scl_o <= 1'b0; acki <= 1'b0; end
        17: begin sda_d <= nul; scl_o <= 1'b1; end
        18: begin sda_d <= nul; scl_o <= 1'b1; end
        19: begin sda_d <= set; scl_o <= 1'b1; end
        // start condition
      STRT: begin sda_d <= set; scl_o <= 1'b1; end
        21: begin sda_d <= set; scl_o <= 1'b1; end
        22: begin sda_d <= nul; scl_o <= 1'b1; end
        23: begin sda_d <= nul; scl_o <= 1'b0; end 
      endcase
      
      // At end of wire cycle, goto to next protocol step,
      // or to STOP if the slave did not ACK the transfer
      if ((t[1:0]==2'd3) & (bctr==3'd0)) t <= (acki) ? STOP : bus;

    end
  end

  // Generate timing signals for protocol engine
  //
  wire sync  = tick & (bus==XFER); // sync sda_m (output i2c_dir)
  wire shift = tick & (t==5'd11);  // shift next output bit
  wire grab  = tick & (t==4'd10);  // grab/shift next input bit

  wire tock  = tick & (t[1:0]==2'd2) & (bctr==3'd0);  // tock = step protocol engine

  // ---------------------------------------------------------------------
  // ------------------------ I2C protocol engine ------------------------
  // ---------------------------------------------------------------------

  localparam read = 1'b1, write = 1'b0;

  reg  xfr_start;   // protocol start
  wire xfr_dir;     // direction (i.e. R/W)
  reg  [4:0] u = 0; // protocol state counter

  // Protocol steps
  //
  always @(posedge clk)
  if (rst) begin
    u <= 0;
  end
  else if (tock) begin
    u <= u + 1;
    case (u)
      // reset sequence: clock until sda goes high and
      // then issue start/stop to reset all devices
       0: begin bus <= IDLE; end
       1: begin bus <= PURG; end
       2: begin bus <= IDLE; if (!sda_i) u <= 1; end
       3: begin bus <= STRT; end
       4: begin bus <= STOP; end
      // wait for a transaction start
       5: begin bus <= IDLE; if (!xfr_start) u <= 5; end
      // write address & subaddress
       6: begin bus <= STRT; acko <= 1'b0; end
       7: begin bus <= XFER; dir <= write; end
       8: begin bus <= ACKN; end
       9: begin bus <= XFER; dir <= write; end
      10: begin bus <= ACKN; if (xfr_dir==read) u <= 14; end
      // write data byte
      11: begin bus <= XFER; dir <= write; end
      12: begin bus <= ACKN; end
      13: begin bus <= STOP; u <= 5; end
      // read: abort write and read at established subaddress
      14: begin bus <= STOP; end
      15: begin bus <= STRT; end
      16: begin bus <= XFER; dir <= write; end
      17: begin bus <= ACKN; end
      18: begin bus <= XFER; dir <= read;  end
      19: begin bus <= ACKN; acko <= 1'b1; end
      20: begin bus <= STOP; u <= 5; end
    endcase
    if (acki) u <= 5; // on slave error, go back to idle
  end
  
  // Handle the data transfers and errors
  //
  reg [7:0] data_o, data_i, data;
  reg       dato, dir;
  
  always @(posedge clk) begin
    if (shift) data_o <= { data_o[6:0], 1'b0 }; // shift output register
    if (grab)  data_i <= { data_i[6:0], sda  }; // shift input register
    if (tick)  begin
      dato <= data_o[7]; // delay data out and direction by
      i2c_dir <= dir;    // one tick, to match wire engine state
    end
    if (tock) begin
      case (u)
       7:    data_o <= { adr[14:8], 1'b0 };  // load device address, W
       16:   data_o <= { adr[14:8], 1'b1 };  // load device address, R
       9:    data_o <=   adr[ 7:0];          // load sub-address
       11:   data_o <=   cmd[ 7:0];          // load output byte
       19:   data   <= { data_i[6:0], sda }; // store input byte
      endcase
    end
  end

  // ---------------------------------------------------------------------
  // ----------------------- Retro CPU interface -------------------------
  // ---------------------------------------------------------------------

  // Interface is through two 16-bit registers, 'cmd/sts' and 'adr'
  //
  //  bit    15    14----------8   7---------------------0
  //       +-----+---------------+-------------------------+
  //  cmd  | r/w | - - - - - - - | do do do do do do do do |   rs = 0, write
  //       +-----+---------------+-------------------------+
  //  sts  | bsy | E 0 e e e e e | di di di di di di di di |   rs = 0, read
  //       +-----+---------------+-------------------------+
  //  adr  |  X  | d d d d d d d | sa sa sa sa sa sa sa sa |   rs = 1, r/w
  //       +-----+---------------+-------------------------+

  reg  [15:0] cmd, adr;
  wire [15:0] sts = { bsy, err, 1'b0, errno, data };
  wire        bsy = (u!=5) | xfr_start;

  assign xfr_dir = cmd[15];

  reg       err;    // error flag
  reg [4:0] errno;  // error number (= protocol state at time of error)
  
  always @(posedge clk) begin
  
    // latch slave error for status
    err <= acki | err;
    if (acki & !err) errno <= u;

    if (!csn & !rdn ) begin
      if (rs) do <= adr; else do <= sts;
    end

    if (!csn & !wrn ) begin
      if (rs)
        adr <= di;
      else begin
        cmd <= di;
        xfr_start <= 1'b1;
        err <= 1'b0; errno <= 5'd0;
      end
    end
    
    if (u!=5) xfr_start <= 1'b0;

  end

endmodule
