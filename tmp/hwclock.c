#include <stdio.h>
#include <time.h>

/* define access to the I2C controller */

static int
dummy()
{
  asm(".globl _i2c");
  asm("_i2c = 0xfec0");
  return 1;
}

struct I2C {
  int cmd;
  int adr;
};

extern struct I2C i2c;

#define DEV   (0x6f00)
#define READ  (0x8000)
#define WRITE (0x0000)
#define sts   cmd

int
get(a)
  int a;
{
  i2c.adr = DEV + a;
  i2c.cmd = READ;
  while( i2c.sts<0 );
  return i2c.sts;
}

int
put(a,v)
  int a,v;
{
  i2c.adr = DEV + a;
  i2c.cmd = WRITE + v;
  while( i2c.sts<0 );
  return i2c.sts;
}

/* decomposed and Unix time storage */
struct tm tmval;
long tm;

/* read decomposed time from RTC */
rd_time()
{
  register int v, check;
  int r[7];
  
  /* repeat as needed to deal with roll-over */
  do {
    v = get(0); r[0] = (v&0xf) + 10 * ((v&0x70)>>4);
    check = v;
    v = get(1); r[1] = (v&0xf) + 10 * ((v&0x70)>>4);
    v = get(2); r[2] = (v&0xf) + 10 * ((v&0x30)>>4);
    v = get(4); r[4] = (v&0xf) + 10 * ((v&0x30)>>4);
    v = get(5); r[5] = (v&0xf) + 10 * ((v&0x10)>>4);
    v = get(6); r[6] = (v&0xf) + 10 * ((v&0xf0)>>4);
    v = get(0);   
  } while (v != check);
  tmval.tm_sec  = r[0];
  tmval.tm_min  = r[1];
  tmval.tm_hour = r[2];
  tmval.tm_mday = r[4];
  tmval.tm_mon  = r[5] - 1;
  tmval.tm_year = r[6] + 100;
}

/* write Unix time to RTC */
wr_time()
{
  register struct tm *tvp;
  register int v;

  tvp = gmtime(&tm);
  put(0, 0x00); /* stop clock */
  put(7, 0x00); /* internal osc, no alarms, MFP off */
  v = tvp->tm_min;  v = (((v/10)<<4) + (v%10));
  put(1, v); /* set minutes */
  v = tvp->tm_hour; v = (((v/10)<<4) + (v%10));
  put(2, v); /* set hours, 24H mode */
  v = tvp->tm_wday; v = (v&0x07) | 0x08;
  put(3, v); /* set weekday, enable battery */
  v = tvp->tm_mday; v = (((v/10)<<4) + (v%10));
  put(4, v); /* set date */
  v = tvp->tm_mon + 1;  v = (((v/10)<<4) + (v%10));
  put(5, v); /* set month */  
  v = tvp->tm_year; v -= 100; v = (((v/10)<<4) + (v%10));
  put(6, v); /* set year */  
  v = tvp->tm_sec;  v = (((v/10)<<4) + (v%10)) | 0x80;
  put(0, v); /* set seconds + restart clock */
}

/* Convert tmval to Unix time */

int	dmsize[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

int dysize(y)
  int y;
{
  if (y % 4 == 0 && (y % 100 != 0 || y % 400 == 0))
    return 366;
  return 365;
}

void
mk_time()
{
  register int i, yr, days, mon;
  
  tm = 0;

  /* calculate days since 1-1-1970 */
  yr = 1900 + tmval.tm_year;
  for(i=1970; i<yr; i++) {
    days = dysize(i);
    tm += days;
  }
  if (days == 366)
    dmsize[1] = 29;
  else
    dmsize[1] = 28;
  mon = tmval.tm_mon + 1;
  while (--mon)
    tm += dmsize[mon-1];
  tm += (tmval.tm_mday - 2);
  
  /* now convert to seconds and add seconds in current day */
  tm *= 24; tm += tmval.tm_hour;
  tm *= 60; tm += tmval.tm_min;
  tm *= 60; tm += tmval.tm_sec;
}

int
main(argc, argv)
  int    argc;
  char **argv;
{
  if(argc==1) goto err;

  for (; argc>1 && argv[1][0]=='-'; argc--, argv++) {
    switch (argv[1][1]) {

    case 'r':
      rd_time();
      mk_time();
      printf("RTC time: %s", ctime(&tm));
      break;

    case 'w':
      printf("writing RTC time\n");
      time(&tm);
      wr_time();
      break;

    case 's':
      printf("setting system time from RTC\n");
      if( (get(0) & 0x80)==0 ) {
        printf("RTC not running\n");
        return 1;
      }
      else {
        rd_time();
        mk_time();
        if (stime(&tm) < 0) {
          printf("no permission\n");
          return 1;
        }
      }
      break;

    default:
      goto err;
    }
  }
  return 0;

err:
  printf("Usage: hwclock -[r|w|s]\n\n");
  printf("-r: read RTC time\n");
  printf("-w: write system time to RTC\n");
  printf("-s: set system time to RTC time\n");
  return 1;
}
