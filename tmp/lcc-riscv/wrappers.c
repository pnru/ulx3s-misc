#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>

#define AT_FDCWD (-100)
#define NULL ((void*)0)

/* Open FILE with access OFLAG.  If O_CREAT is in OFLAG,
  a third argument is the file protection.  */
int
open(const char *file, int oflag, ...)
{
	int mode;

	if (file == NULL) {
		errno = EINVAL;
		return -1;
	}

	if (oflag & (O_CREAT)) {
		va_list arg;
		va_start(arg, oflag);
		mode = va_arg(arg, int);
		va_end(arg);
	}

	return _openat(AT_FDCWD, file, oflag, mode);
}