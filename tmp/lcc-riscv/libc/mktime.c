/* Convert tmval to Unix time */

struct tm tmval;

int dmsize[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

int dysize(int y)
{
  if (y % 4 == 0 && (y % 100 != 0 || y % 400 == 0))
    return 366;
  return 365;
}

unsigned long mk_time(void)
{
  int i, yr, mon;
  unsigned long tm = 0;

  /* calculate days since 1-1-1970 */
  yr = 1900 + tmval.tm_year;
  for(i=1970; i<yr; i++) {
    tm += dysize(i);
  }
  if (dysize(yr) == 366)
    dmsize[1] = 29;
  else
    dmsize[1] = 28;
  mon = tmval.tm_mon + 1;
  while (--mon)
    tm += dmsize[mon-1];
  tm += (tmval.tm_mday - 1);
  /* now convert to seconds and add seconds in current day */
  tm *= 24; tm += tmval.tm_hour;
  tm *= 60; tm += tmval.tm_min;
  tm *= 60; tm += tmval.tm_sec;
  return tm;
}
