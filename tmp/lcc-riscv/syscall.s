        .text
	.globl errno
        .align  4

; write(fd, buf, len)
;
	.globl write
write:
        mv x10,x12
        mv x11,x13
        mv x12,x14
        li x17,17
        ecall
cerror:
        beq x11,x0,L.1
        la x10,errno
        sw x11,0(x10)
        li x10,-1
L.1:
        jalr x0,x1,0

; read(fd, buf, len);
;
	.globl read
read:
	mv x10,x12
	mv x11,x13
	mv x12,x14
	li x17,17
	ecall
	beq x10,x10,cerror

; open(path, mode)
;
	.globl open
open:
	mv x10,x12
	mv x11,x13
	li x17,17
	ecall
	beq x10,x10,cerror

; _exit(status)
;
	.globl _exit
_exit:
	mv x10,x12
	li x17,97
	ecall

; <etc>
;

