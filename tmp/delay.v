module sys
(
  input  wire        clk_25mhz,
  output wire        ftdi_rxd
);

  reg t = 0;

  always @(posedge clk_25mhz) t <= ~t;

  DELAYG #(.DEL_VALUE(60)) clk_dly(.A(t), .Z(ftdi_rxd));

endmodule
