`default_nettype none

module SVGA(
    input  wire pixclk,        // pixel clock
    input  wire rst,           // reset: restarts frame
    output wire hsync,         // horizontal sync
    output wire vsync,         // vertical sync
    output wire vde,           // high during active pixel drawing
    output wire [10:0] xctr,   // current pixel x position
    output wire  [9:0] yctr    // current pixel y position
    );

    // SVGA 800x600, pixclk 40Mhz
    localparam HS_STA = 40;              // horizontal sync start
    localparam HS_END = 40 + 128;        // horizontal sync end
    localparam HA_STA = 40 + 128 + 88;   // horizontal active pixel start
    localparam LINE   = 1056;            // complete line (pixels)
    localparam VS_STA = 600 + 1;         // vertical sync start
    localparam VS_END = 600 + 1 + 4;     // vertical sync end
    localparam VA_END = 600;             // vertical active pixel end
    localparam SCREEN = 628;             // complete screen (lines)

    reg [10:0] h_count; // line position
    reg  [9:0] v_count; // screen position

    // video frame counters
    always @ (posedge pixclk)
    begin
      if (rst) begin
        h_count <= 0;
        v_count <= 0;
      end
      else begin
        if (h_count == LINE-1) begin
          h_count <= 0;
          v_count <= v_count + 1;
        end
        else 
          h_count <= h_count + 1;
    
        if (v_count == SCREEN-1)
          v_count <= 0;
      end
    end

    // generate sync signals (active high for 800x600)
    assign hsync = ((h_count >= HS_STA) & (h_count < HS_END));
    assign vsync = ((v_count >= VS_STA) & (v_count < VS_END));

    // keep x and y bound within the active pixels
    assign xctr = (h_count <  HA_STA) ? 0 : (h_count - HA_STA);
    assign yctr = (v_count >= VA_END) ? (VA_END - 1) : (v_count);

    // active: high during active pixel drawing
    assign vde = ~((h_count < HA_STA) | (v_count > VA_END - 1)); 
    
endmodule
