  // ESP32->ECP5 serial receiver fragment

  // Bit timer for 115200 baud @ 25 MHz clock
  //
  reg [7:0] rx_timer = 0;

  wire rbit_clk = !(|rx_timer);
  reg ld_rx_tmr;

  always @(posedge clk) begin
    if (ld_rx_tmr)
      rx_timer <= 325; // 1.5 bit time
    else if (rbit_clk)
      rx_timer <= 217; // 1 bit time 
    else
      rx_timer <= rx_timer - 1;
  end

  // sync host_rx
  reg rx;
  always @(posedge clk) rx = host_rx;

  // Receiver side of uart (start - 8 data - stop)
  //
  reg [7:0] shift  = 0, data;
  reg [3:0] bitcnt = 0;
  reg strobe = 0;

  always @(posedge clk)
  begin
    strobe <= 0; ld_rx_tmr <= 0;
    if (bitcnt==0) begin
      // wait for start bit
      if (!rx) begin
        bitcnt <= 1;
        ld_rx_tmr <= 1;
        end
      end
    else if (rbit_clk) begin
      // shift in 8 bits
      if (bitcnt<9) begin // !! works in Icarus, synthesis needs <10
        shift  <= { rx, shift[7:1] };
        bitcnt <= bitcnt + 1;
        end
      // latch byte
      else begin
        bitcnt <= 0;
        strobe <= 1;
        data <= shift;
      end
    end
  end