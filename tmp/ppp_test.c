#include <stdio.h>
#include <unistd.h>

typedef unsigned char uchar;

#define ESC   0
#define NOESC 1
int escmode;

#define GET32(buf, i) ((buf[i]<<24) + (buf[i+1]<<16) + (buf[i+2]<<8) + buf[i+3])
#define GET16(buf, i) ((buf[i]<<8) + buf[i+1])

char* names[] = { "", "Req", "Ack", "Nak", "Rej","Term", "T.Ack", "7", "8" };

void print_frame(uchar *buf, int n)
{
  int i, len, end, proto;

  proto = GET16(buf, 2);
  printf("address: %02x, code: %02x\n", buf[0], buf[1]);
  printf("protocol: %04x\n", proto);
  switch( proto ) {
  case 0xc021: printf("LCP  "); break;
  case 0x8021: printf("IPCP "); break;
  case 0x0021: printf("IP   "); break;
  }
  len = GET16(buf, 6);
  end = len + 4;
  if (n!=end) printf("BAD LENGTH!\n");
  printf("msg: %02x (%s), id: %02x, len: %d\n", buf[4], names[buf[4]], buf[5], len);
  switch (buf[4]) {
  case 1: // ConfReq
  case 2: // ConfAck
  case 3: // ConfNack
  case 4: // ConfRej
    printf("options:\n");
    for(i=8; i<end; ) {
      printf(" %02x %02x ", buf[i], buf[i+1]);
      i++;
      switch (buf[i]) {
      case 2: printf("\n");                       i += 1; break;
      case 3: printf(" %02x\n", buf[i+1]);        i += 2; break;
      case 4: printf(" %04x\n", GET16(buf, i+1)); i += 3; break;
      case 6: printf(" %08x\n", GET32(buf, i+1)); i += 5; break;
      }
    }
    printf("\n");
    break;
  case 5: // TermReq
    for(i=8; i<end; ) {
      printf("%c", buf[i++]);
    }
    printf("\n");
    break;
  default:
    printf("\n");
    break;
  }
}

void write_frame(int fd, uchar *buf, int len);

void IP_frame(int fd, uchar *buf, int n)
{
  int i;

  printf("IP frame received:\n");
  for(i=1; i<n; i++)
    printf("%02x ", buf[i]);
  printf("\n");
}

// IP setup
//
uchar IPCPReq[] = {
  /* our request for IP address 172.16.0.2 */
  0xff, 0x03, 0x80, 0x21, 0x01, 0x01, 0x00, 0x0a,
  0x03, 0x06, 0xac, 0x10, 0x00, 0x02
};

uchar IPCPRej[] = {
  /* reject TCP header compression + DNS requests */
  0xff, 0x03, 0x80, 0x21, 0x04, 0x01, 0x00, 0x16,
  0x02, 0x06, 0x00, 0x2d, 0x0f, 0x01,
  0x81, 0x06, 0x00, 0x00, 0x00, 0x00,
  0x83, 0x06, 0x00, 0x00, 0x00, 0x00  
};

uchar IPCPNak[] = {
  /* suggest IP address 172.16.0.1 for ESP32 */
  0xff, 0x03, 0x80, 0x21, 0x03, 0x02, 0x00, 0xa,
  0x03, 0x06, 0xac, 0x10, 0x00, 0x01
};

void IPCP_frame(int fd, uchar *buf, int n)
{
  print_frame(buf, n);
  
  switch( buf[4] ) {
  
  case 1: /* ConfReq: reject compression, suggest IP addresses, then accept */
    if (buf[8]==2) {
      write_frame(fd, IPCPRej, sizeof(IPCPRej));
    } else if (buf[10]==0) {
      write_frame(fd, IPCPNak, sizeof(IPCPNak));
    } else {
      buf[4] = 2;
      write_frame(fd, buf, n);
      write_frame(fd, IPCPReq, sizeof(IPCPReq));
    }
    break;
  case 2: /* ConfAck: connection OK, stop escaping ctrl chars */
    escmode = NOESC;
    break;
  case 5: /* TermReq: acknowledge & connection down */
    escmode = ESC;
    buf[4] = 6;
    write_frame(fd, buf, n);
    break;
  default: /* other: do nothing */
    break;
  }
}

// Link management
//
uchar LCPReq[] = {
  0xff, 0x03, 0xc0, 0x21, 0x01, 0x01, 0x00, 0x14,
  0x02, 0x06, 0x00, 0x00, 0x00, 0x00, 0x05, 0x06,
  0x80, 0x81, 0x82, 0x83, 0x07, 0x02, 0x08, 0x02
};

void LCP_frame(int fd, uchar *buf, int n)
{
  print_frame(buf, n);

  switch( buf[4] ) {

  case 1: /* ConfReq: acknowledge request & send our request */
          buf[4] = 2;
          write_frame(fd, buf, n);
          write_frame(fd, LCPReq, sizeof(LCPReq));
          break;
  case 2: /* ConfAck: connection established */
          break;
  case 5: /* TermReq: acknowledge & connection down */
          escmode = ESC;
          buf[4] = 6;
          write_frame(fd, buf, n);
          break;
  default: /* other: do nothing */
          break;
  }
}

void process_frame(int fd, uchar *buf, int len)
{
  int i = 0;
  
  // with PPP compression on, the 4 byte HDLC header collapses
  // to one byte for regular IP frames.
  if (buf[0]==0x21) {
    IP_frame(fd, buf, len);
    return;
  }
  if (GET16(buf,0)==0xff03) i = 2;
  switch ( GET16(buf, i) ) {
    case 0xc021: LCP_frame(fd, buf, len);  break;  // link control protocol
    case 0x8021: IPCP_frame(fd, buf, len); break;  // IPv4 control protocol
    case 0x8057:                           break;  // ignore IPv6 stuff 
    case 0x0021: IP_frame(fd, buf, len);   break;  // IP itself
    default:
      printf("Unhandled frame type\n");
      for(i=0; i<len; i++)
        printf("%02x ", buf[i]);
      printf("\n");
  }
}

//  Low Level routines:
//  - serial port configuration
//  - byte stuffing
//  - framing & crc calculation

#include <stdlib.h>
#include <fcntl.h>
#include <termios.h>

#define PORT "/dev/cu.usbserial-DN05L6UD"

#include <sys/ioctl.h>
#include <IOKit/serial/ioss.h>

struct termios sio_cfg;

int open_serial()
{
  int fd;
  speed_t baud = 115200;

  fd = open(PORT, O_RDWR);
  if (fd<0 ) {
    printf("could not open serial link\n");
    exit(1);
  }
  
  cfmakeraw(&sio_cfg);
  sio_cfg.c_cflag = CS8;
  tcsetattr(fd, TCSANOW, &sio_cfg);
  ioctl(fd, IOSSIOSPEED, &baud);
  
  printf("opened ppp line ('%s', fd=%d):\n", PORT, fd);
  return fd;
}

#define FRAME  0x7e

uchar get_byte(int fd)
{
  uchar c;
  read(fd, &c, 1);
  if (c==0x7d) {
    read(fd, &c, 1);
    if (c!=FRAME) c ^= 0x20;
  }
  return c;
}

void put_byte(int fd, uchar c)
{
  uchar e = 0x7d;

  if (c==0x7e || c==0x7d || (escmode==ESC && c<0x20)) {
    write(fd, &e, 1);
    c ^= 0x20;
  }
  write(fd, &c, 1);
}

int fcs;

void crc(uchar c)
{
  int i;

  i  = (fcs^c) & 0xff;
  i ^= (i<<4)  & 0xff;
  fcs = (fcs>>8) ^ (i<<8) ^ (i<<3) ^ (i>>4);
}

void write_frame(int fd, uchar *buf, int len)
{
  int i;
  uchar c, f = FRAME;

  write(fd, &f, 1);
  fcs = 0xffff;
  for(i=0; i<len; i++) {
    c = buf[i];
    put_byte(fd, c);
    crc(c);
  }
  fcs = ~fcs;
  c =  fcs     & 0xff; put_byte(fd, c);
  c = (fcs>>8) & 0xff; put_byte(fd, c);
  write(fd, &f, 1);
  printf("SENT\n");
  print_frame(buf, len);
}

int read_frame(int fd, uchar *buf)
{
  int i;
  uchar c = FRAME;

  while (c==FRAME)
    c = get_byte(fd);

  fcs = 0xffff;
  i = 0;
  while (1) {
    if (c==FRAME) break;
    buf[i++] = c;
    crc(c);
    c = get_byte(fd);
  }
  return (fcs==0xf0b8) ? i-2 : -1;
}

#define BUFSIZ 1024
uchar buf[BUFSIZ];

int main()
{
  int fd, n, i, done;
  uchar c = 0;

  fd = open_serial();
  while (c!=FRAME)
    c = get_byte(fd);

  while(1) {
    i = read_frame(fd, buf);
    if (i>0) process_frame(fd, buf, i);
  }
}