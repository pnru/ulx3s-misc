//
// Simplistic 8192x16 ROM module
//
// This source code is public domain
//

module ROM(
  input wire CLK,
  input wire nCS,
  input wire [12:0] ADDR,
  output reg [15:0] DO
);
  
  // Implementation
  reg [15:0] mem[0:8191];
  
  always @(posedge CLK)
  begin
    if (!nCS) begin
      DO <= mem[ADDR];
    end
  end
  
  initial
  begin
    $readmemh("rom.mem", mem); // 16KB rom set
  end
  
endmodule