//
// Simplistic 130672x16 RAM module
//
// This source code is public domain
//

module RAM(
  input wire CLK,
  input wire nCS,
  input wire nWE,
  input wire nLDS,
  input wire nUDS,
  input wire [16:0] ADDR,
  input wire [15:0] DI,
  output reg [15:0] DO
);

  // Implementation
  reg [15:0] mem[0:130671];
  
  always @(posedge CLK)
  begin
    if (!nCS) begin
      if (!nWE & !nLDS) mem[ADDR][7:0]  <= DI[7:0];
      if (!nWE & !nUDS) mem[ADDR][15:8] <= DI[15:8];
    end
  end

  always @(posedge CLK)
  begin
    if (!nCS) begin
      DO <= mem[ADDR];
    end
  end

`ifdef __ICARUS__
  initial begin : prefill
    integer i;
    for(i=0; i<130672; i=i+1) mem[i] = 0;
  end
`endif
 
endmodule
