`timescale 1ns / 1ps
// 1024x768 display controller NW/PR 24.1.2014

module VIDEO (
  input  wire        clk,
  input  wire        pclk,
  input  wire        inv,
  input  wire [31:0] cpudata,
  input  wire [23:0] cpuadr,
  input  wire [ 3:0] wmask,

  output wire        hsync,
  output wire        vsync,
  output wire        rgb,
  output wire        vde
);

  reg  [10:0] hcnt = 0;
  reg   [9:0] vcnt = 0;
  reg  [31:0] pixbuf;
  reg         hblank = 0;
  wire        hend, vend, vblank, xfer, vid;
  reg  [14:0] vidadr;
  wire [31:0] viddata;

  assign hend   = (hcnt == 1343), vend = (vcnt == 806);
  assign vblank = (vcnt[8] & vcnt[9]);  // (vcnt >= 768)
  assign hsync  = ((hcnt >= 1048+6) & (hcnt < 1184+6));
  assign vsync  = (vcnt >= 771) & (vcnt < 774);
  assign xfer   = (hcnt[4:0] == 6);    // data delay > hcnt cycle + req cycle
  assign rgb    = (pixbuf[0] ^ inv) & ~hblank & ~vblank;
  assign vde    = ~(hblank|vblank);

  always @(posedge pclk) begin  // pixel clock domain
    hcnt   <= hend ? 0 : hcnt+1;
    vcnt   <= hend ? (vend ? 0 : (vcnt+1)) : vcnt;
    hblank <= xfer ? hcnt[10] : hblank;  // hcnt >= 1024
    pixbuf <= xfer ? viddata : {1'b0, pixbuf[31:1]};
    vidadr <= {vcnt, hcnt[9:5]};
  end

  // pipeline address arithmetic
  //
  reg  [19:0] adr, wr_adr;
  reg  [31:0] data;
  reg   [3:0] wm;
  always @(clk) begin
    adr    <= cpuadr[19:0];
    data   <= cpudata;
    wm     <= wmask;
    wr_adr <= {~adr[19:7], adr[6:0]} - 20'h00100;
  end

  wire ce = wr_adr[19:17]==3'b000 & wr_adr[16:15]!=2'b11;
  
  VRAM vram (
    // CPU port
    .a_clk(clk),
    .a_addr(wr_adr[16:2]),
    .a_din(data),
    .a_ce(ce), .a_we(wm),

    // Video port  
    .b_clk(pclk),
    .b_addr(vidadr),
    .b_dout(viddata)
  );

endmodule

module VRAM (
  // Port A
  input  wire        a_clk,
  input  wire [14:0] a_addr,
  input  wire [31:0] a_din,
  input  wire        a_ce,
  input  wire  [3:0] a_we,
  
  // Port B
  input  wire        b_clk,
  input  wire [14:0] b_addr,
  output reg  [31:0] b_dout
);

  reg [31:0] mem[0:24575];

  // Port A access
  always @(posedge a_clk) begin
      if (a_ce & a_we[3]) mem[a_addr][31:24] <= a_din[31:24];
      if (a_ce & a_we[2]) mem[a_addr][23:16] <= a_din[23:16];
      if (a_ce & a_we[1]) mem[a_addr][15: 8] <= a_din[15: 8];
      if (a_ce & a_we[0]) mem[a_addr][ 7: 0] <= a_din[ 7: 0];
  end

  // Port B access
  always @(posedge b_clk) begin
      b_dout <= mem[b_addr];
  end

endmodule
