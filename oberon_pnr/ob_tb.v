`timescale 1ns/1ns

module sys_tb;

	reg  sysclk = 0;
	always #20 sysclk = !sysclk;
	
	wire [3:0] sd_d;
  wire       sd_clk, sd_cmd;
  
  wire [15:0] sd_data;
  wire [12:0] sd_addr;
  wire sd_csn, sd_wen, sd_rasn, sd_casn, clk_sdr, sd_cke;
  wire [1:0] sd_ba, sd_dqm;
  wire stop;
  
	// Oberon ULX3S
	RISC5_ulx3s tut(
		.clk_25mhz(sysclk),
		.btn(7'h01),

    .sdram_d(sd_data),
		.sdram_a(sd_addr),
    .sdram_ba(sd_ba),
    .sdram_dqm(sd_dqm),
    .sdram_csn(sd_csn),
		.sdram_wen(sd_wen),
		.sdram_rasn(sd_rasn),
		.sdram_casn(sd_casn),
		.sdram_clk(clk_sdr),
    .sdram_cke(sd_cke),
    
		.sd_d(sd_d),
    .sd_clk(sd_clk),
    .sd_cmd(sd_cmd),
    
    .stop(stop)
	);
///*
  // simulated SDRAM, only 1024KB
	SDRAM_TB sdram(
	  .sdram_d(sd_data),
	  .sdram_a(sd_addr),
	  .sdram_csn(sd_csn),
	  .sdram_wen(sd_wen),
	  .sdram_rasn(sd_rasn),
	  .sdram_casn(sd_casn),
	  .sdram_clk(clk_sdr)
  );
//*/
/*
  mt48lc16m16a2 sdram (
    .Dq(sd_data),
    .Addr(sd_addr),
    .Ba(sd_ba),
    .Clk(clk_sdr),
    .Cke(sd_cke),
    .Cs_n(sd_csn),
    .Ras_n(sd_rasn),
    .Cas_n(sd_casn),
    .We_n(sd_wen),
    .Dqm(sd_dqm)
  );
*/
  // Emulate SD Card
  SDCARD sdcard (
    .clk(sysclk),
    .SCLK(sd_clk),
    .MOSI(sd_cmd),
    .MISO(sd_d[0]),
    .SS(sd_d[3])
  );
  
  integer i;
	initial begin
		$dumpfile("sys.vcd");
		$dumpvars(0,sys_tb);
    for(i=0; i<1 && !stop; i=i+1) begin
      $display("ms = %d", i);
      if (i==10) $dumpoff();
      if (i==90) $dumpon();
		  #1_000_000;
    end
    $writememh("sdram.txt", sdram.mem);
    //$writememh("sdram.txt", sdram.Bank0);
    $writememh("cram.txt",  tut.cache.cram.mem);
    //$writememh("tags.txt",  tut.cache.tags);
    $finish;
	end

endmodule

module SDCARD (
  input  wire clk,
  input  wire SCLK,
  input  wire MOSI,
  output wire MISO,
  input  wire SS
);
  integer fh;

  initial begin
    fh = $fopen("disk/ledboot.img", "a+b");
  end

  reg [47:0] in = -1;
  reg [47:0] cmd;

  always @(posedge SCLK) begin
    if (sync) begin
      in  <= 48'hffffffffffff;
    end
    else
      in <= { in[46:0], MOSI };
  end
  
  wire sync = !in[47];
  
  reg [7:0] out;

  always @(posedge SCLK) begin
    if (sync) begin
      cmd <= in;
      out <= 8'h00;
      $display("SD CMD = %h", in);
    end
    else
      out <= { out[6:0], 1'b1 };
  end
  
  wire rmod = (in[45:40]==5'h11) ? 1 : 0;
  wire read = rmod | !holding;
  
  reg [12:0] ctr = 513;
  reg  [7:0] byte;
  reg        holding = 1'b1;

  integer n, sec;

  always @(negedge SCLK) begin
    if (rmod & holding) begin
      ctr     <= 0;
      byte    <= 0;
      holding <= 1'b0;
      sec = (in[39:8]) * 512;
      n = $fseek(fh, sec, 0);
    end
    else if (!holding) begin
      ctr <= ctr + 1;
      byte <= { byte[6:0], 1'b1 };
      
      if (ctr[2:0]==3'b111) begin
        case (ctr[12:3])
              0:  byte <= 8'hfe;
        default:  begin
                  n = $fread(byte, fh);
                  //$fwrite(fh, "%c", data);
                  //$write("%x", byte);
                  byte <= byte;
                  end
            513:  holding <= 1'b1;
        endcase
      end
    end
  end

  assign MISO = (!read) ? out[7] : byte[7];

endmodule



module SDRAM_TB (
  input  wire        sdram_clk,
  input  wire        sdram_cke,
  input  wire        sdram_csn,
  input  wire        sdram_wen,
  input  wire        sdram_rasn,
  input  wire        sdram_casn,
  input  wire  [1:0] sdram_ba,
  input  wire [12:0] sdram_a,
  inout  wire [15:0] sdram_d,
  input  wire  [1:0] sdram_dqm
);

  reg  [15:0] mem[0:524287];
  
  // compose the command
  wire  [3:0] cmd;
  assign cmd[0] = sdram_wen;
  assign cmd[1] = sdram_casn;
  assign cmd[2] = sdram_rasn;
  assign cmd[3] = sdram_csn;

  // all possible commands
  localparam CMD_INHIBIT         = 4'b1111;
  localparam CMD_NOP             = 4'b0111;
  localparam CMD_ACTIVE          = 4'b0011;
  localparam CMD_READ            = 4'b0101;
  localparam CMD_WRITE           = 4'b0100;
  localparam CMD_BURST_TERMINATE = 4'b0110;
  localparam CMD_PRECHARGE       = 4'b0010;
  localparam CMD_AUTO_REFRESH    = 4'b0001;
  localparam CMD_LOAD_MODE       = 4'b0000;

  reg  [2:0] rd_sh = 0;
  reg [15:0] data;
  reg [18:0] ab;
  reg        rd = 0 , wr = 0;

  always @(posedge sdram_clk) begin
  
    // default settings; data out has 2ns hold time
    rd_sh   <= { rd_sh[1:0], rd };

    // handle command
    case (cmd)
    
    CMD_ACTIVE:
      begin
        ab[18:8] <= sdram_a[10:0];
      end

    CMD_READ:
      begin
        ab[7:0] <= sdram_a[7:0];
        rd_sh <= 3'b001;
        rd <= 1'b1;
      end
      
    CMD_WRITE:
      begin
        ab[7:0] <= sdram_a[7:0];
        data    <= sdram_d;
        wr <= 1'b1;
      end
    
    CMD_AUTO_REFRESH:
      ; /* no need to refresh block RAM */

    CMD_BURST_TERMINATE:
      begin
        rd <= 1'b0;
        wr <= 1'b0;
      end

    CMD_PRECHARGE,      
    CMD_LOAD_MODE:
      ; /* ignore for now */

    CMD_INHIBIT,        
    CMD_NOP:
      ; /* do nothing */

    default:
      ; /* do nothing */

    endcase
    
    // write data, latch in data on the next clock;
    if (wr) begin
      mem[ab] <= data;
      data    <= sdram_d;
      ab <= ab + 1;
    end
    
    if (rd_sh[1]) begin
      data <= mem[ab];
      ab <= ab + 1;
    end;

  end
  
  // read data, latency is 2 clocks + 7ns for CAS LATENCY==3
  // hold time is 2ns
  //
  assign #7 sdram_d = (rd_sh[2]) ? data : 16'bz;

  integer i;
  initial begin
    for(i=0; i<524288; i=i+1) mem[i] = 16'hc000 | (i&12'hfff);
  end

endmodule
