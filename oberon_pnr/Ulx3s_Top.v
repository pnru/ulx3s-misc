`timescale 1ps/1ps

module RISC5_ulx3s (
  input         clk_25mhz,       // h/w clock

  output        wifi_en,         // enable ESP32
  output        wifi_gpio0,      // quiet ESP32

  input   [6:0] btn,             // user buttons
  output  [7:0] led,             // user leds

  output        ftdi_rxd,        // Oberon TX
  input         ftdi_txd,        // Oberon RX

  inout         sd_clk,          // SD_CK
  inout         sd_cmd,          // SD_DI
  inout   [3:0] sd_d,            // [0] = SD_DO, [3] = SD_nCS

  output        usb_fpga_pu_dp,  // US2 PS/2 pull-ups
  output        usb_fpga_pu_dn,
  inout         usb_fpga_dp,     // US2 PS/2 clk+dat
  inout         usb_fpga_dn,

  inout  [27:0] gp,              // GPIO Header pins
  inout  [27:0] gn,
  
  output        sdram_clk,       // SDRAM
  output        sdram_cke,
  output        sdram_csn, 
  output        sdram_rasn,
  output        sdram_casn,
  output        sdram_wen,
  output [12:0] sdram_a,
  output  [1:0] sdram_ba,
  output  [1:0] sdram_dqm,
  inout  [15:0] sdram_d,
  
  output  [3:0] gpdi_dp         // DVID Video
`ifdef __ICARUS__
  , output reg  stop = 0        // stop sim run
`endif
);
`ifdef __ICARUS__
  wire clk = stop ? clk : clk_25mhz;
`else
  wire clk;
`endif

  // ULX3S specific things
  //
  assign wifi_gpio0     = btn[0];   // disable ESP32 monitor
  assign wifi_en        = 1'b0;     // disable ESP32
  assign usb_fpga_pu_dp = 1'b1;     // US1 D+ pull to +3.3V through 1.5K resistor
  assign usb_fpga_pu_dn = 1'b1;     // US1 D- pull as above
  assign gp[22]         = 1'b1;     // US3 D+ pull as above
  assign gn[22]         = 1'b1;     // US3 D- pull as above
  assign sd_d[2:1]      = 2'bzz;    // force inout to input

`ifdef __ICARUS__
  reg sdrclk = 0;
  always #5000 sdrclk = !sdrclk;
  reg pixclk = 0;
  always #1538 pixclk = !pixclk;
  reg pix5clk = 0;
  always #308 pix5clk = !pix5clk;
`else
  wire sdrclk, pixclk, pix5clk;
  PLL1 pll_vid (
    .clkin(clk_25mhz),
    .pll65(pixclk),
    .pll325(pix5clk)
  );
  PLL2 pll_cpu (
    .clkin(clk_25mhz),
    .pll100(sdrclk),
    .pll25(clk),
    .locked(lock)
  );
`endif

  // Long boot reset; not sure why this is needed, but it is
  reg [31:0] rctr = 0;
  always @(posedge clk_25mhz) rctr <= rctr + lrst;
  wire lrst = !rctr[26];
  
  // System timer (@ I/O addr 0) and reset
  //
  wire        limit = (cnt0 == 24999);
  wire        lock;
  reg         rst = 0;
  reg  [15:0] cnt0 = 0;
  reg  [31:0] cnt1 = 0; // milliseconds

  always @(posedge clk) begin
    cnt0 <= limit ? 0 : cnt0 + 1;
    cnt1 <= cnt1 + limit;
`ifdef __ICARUS__
   rst <= (cnt0 > 100) | rst;
`else
   rst <= !lrst & lock & (((cnt1[4:0] == 20) & limit) ? btn[0] : rst);
`endif

  end

  // CPU
  //
  wire [23:0] adr;
  wire [31:0] inbus, inbus0;  // data to RISC core
  wire [31:0] outbus;         // data from RISC core
  wire        rd, wr, ben, mrdy;

  RISC5 cpu (
    .clk(clk),
    .rst(rst),
    .ce(mrdy),
    .stallX(1'b0), //~mrdy),
    .adr(adr),
    .codebus(inbus0),
    .inbus(inbus),
    .outbus(outbus),
    .rd(rd),
    .wr(wr),
    .ben(ben)
  );

  // LEDs @ I/O addr 1
  // 1 r/w = buttons+switches / LEDs
  //
  reg   [7:0] Lreg;
  wire  [3:0] btns = { ~btn[0], btn[3], btn[5], btn[4] };
  wire  [7:0] swi = 8'b0000_0000;
  
  always @(posedge clk) begin
    if (wr & ioenb & (iowadr == 1)) Lreg <= outbus[7:0];
    if (~rst)                       Lreg <= 0;
  end
  
  assign led = Lreg;
  
  // Serial @ I/O addr 2, 3
  // 2 r/w = RS-232 data   / RS-232 data (start)
  // 3 r/w = RS-232 status / RS-232 control

  //
  wire  [7:0] dataRx;
  wire  [7:0] dataTx   = outbus[7:0];
  wire        startTx  = wr & ioenb & (iowadr == 2);
  wire        doneRx   = rd & ioenb & (iowadr == 2);
  wire        rdyRx, rdyTx;
  reg         bitrate = 0;

  always @(posedge clk) begin
    if (wr & ioenb & (iowadr == 3)) bitrate <= outbus[0];
    if (~rst)                       bitrate <= 0;
  end

  RS232R receiver (
    .clk(clk),
    .rst(rst),
    .RxD(ftdi_txd),
    .fsel(bitrate),
    .done(doneRx),
    .data(dataRx),
    .rdy(rdyRx)
  );
  RS232T transmitter (
    .clk(clk),
    .rst(rst),
    .start(startTx),
    .fsel(bitrate),
    .data(dataTx),
    .TxD(ftdi_rxd),
    .rdy(rdyTx)
  );

  // SDCard @ I/O addr 4,5
  // 4 r/w = SPI data / SPI data (start)
  // 5 r/w = SPI status / SPI control
  //
  wire [31:0] spiRx;
  wire        spiRdy;
  reg   [3:0] spiCtrl;
  wire        spiStart = wr & ioenb & (iowadr == 4);

  assign sd_d[3]  = ~spiCtrl[0];

  always @(posedge clk) begin
    if (wr & ioenb & (iowadr == 5)) spiCtrl <= outbus[3:0];
    if (~rst)                       spiCtrl <= 0;
    //if (spiStart) $display("SD<-%h", outbus);
  end

  SPI spi(
    .clk(clk),
    .rst(rst),
    .start(spiStart),
    .dataTx(outbus),
    .fast(spiCtrl[2]),
    .dataRx(spiRx),
    .rdy(spiRdy),
    .SCLK(sd_clk),
    .MOSI(sd_cmd),
    .MISO(sd_d[0])
  );

  // Keyboard / Mouse interface @ I/O addr 6,7
  // 6 r/w = PS2 keyboard / --
  // 7 r/w = mouse / --
  //
  wire  [7:0] dataKbd;
  wire        rdyKbd;
  wire        doneKbd  = rd & ioenb & (iowadr == 7);

  PS2 kbd(
    .clk(clk),
    .rst(rst),
    .done(doneKbd),
    .rdy(rdyKbd),
    .shift(),
    .data(dataKbd),
    .PS2C(usb_fpga_dp),
    .PS2D(usb_fpga_dn)
  );

  wire [27:0] dataMs;
  wire  [2:0] mousebtn;

  MouseM #(.c_x_bits(10), .c_y_bits(10), .c_y_neg(1), .c_z_ena(0), .c_hotplug(1)) Mse (
    .clk(clk),
    .clk_ena(1'b1),
    .ps2m_reset(~rst),
    .ps2m_clk(gp[21]),      // = CLK for U3 (on PMOD)
    .ps2m_dat(gn[21]),      // = DAT for U3 (on PMOD)
    .x(dataMs[9:0]),
    .y(dataMs[21:12]),
    .btn(mousebtn)
  );
  assign dataMs[24] = mousebtn[1]; // left
  assign dataMs[25] = mousebtn[2]; // middle
  assign dataMs[26] = mousebtn[0]; // right
  assign dataMs[27] = 1'b1;
  assign dataMs[23:22] = 2'b00;
  assign dataMs[11:10] = 2'b00;

  // GP I/O @ I/O addr 8,9
  // 8 r/w = general-purpose I/O data
  // 9 r/w = general-purpose I/O tri-state control
  //
  reg   [7:0] gpout, gpoc;
  wire  [7:0] gpin;
  
  always @(posedge clk)
  begin
    if (wr & ioenb & (iowadr == 8)) gpout <= outbus[7:0];
    if (wr & ioenb & (iowadr == 9)) gpoc  <= outbus[7:0];
    if (~rst)                       gpoc  <= 0;
  end

  assign gp[9:2] = gpoc ? gpout : 8'hzz;
  assign gpin = gp[9:2];

  // IO addresses for input / output
  // 0  milliseconds / --
  // 1  switches / LEDs
  // 2  RS-232 data / RS-232 data (start)
  // 3  RS-232 status / RS-232 control
  // 4  SPI data / SPI data (start)
  // 5  SPI status / SPI control
  // 6  PS2 keyboard / --
  // 7  mouse / --
  // 8  general-purpose I/O data
  // 9  general-purpose I/O tri-state control

  // Databus CPU input mux
  //
  wire [3:0] iowadr = adr[5:2];
  wire       ioenb  = (adr[23:6] == 18'h3FFFF);

  assign inbus = ~ioenb ? inbus0 :
                          ((iowadr == 0) ? cnt1 :
                           (iowadr == 1) ? {20'b0, btns, swi} :
                           (iowadr == 2) ? {24'b0, dataRx} :
                           (iowadr == 3) ? {30'b0, rdyTx, rdyRx} :
                           (iowadr == 4) ? spiRx :
                           (iowadr == 5) ? {31'b0, spiRdy} :
                           (iowadr == 6) ? { 3'b0, rdyKbd, dataMs} :
                           (iowadr == 7) ? {24'b0, dataKbd} :
                           (iowadr == 8) ? {24'b0, gpin} :
                           (iowadr == 9) ? {24'b0, gpoc} :
                           0);

  // CACHE + SDRAM
  //
  wire [15:0] sdr_din, sdr_dout;
  wire [11:0] sdr_addr;
  wire sdr_get, sdr_put, sdr_rd, sdr_wr;
  
  // generate byte write mask (all 4 for regular writes)
  wire [3:0] wmask = ({4{!ben}} | (1'b1 << adr[1:0])) & {4{wr}};
  
  wire memsel = !ioenb & !(&adr[23:14]); // &(rd|wr)

  CACHE cache (
    .clk(clk),            // CPU side
    .rst(~rst),

    .addr(adr[19:0]),
    .din(outbus),
    .dout(inbus0),
    .wmask(wmask),
    .mreq(memsel),
    .rdy(mrdy),
  
    .sdr_clk(sdram_clk),   // SDRAM controller side
    .sdr_addr(sdr_addr),
    .sdr_din(sdr_din),
    .sdr_dout(sdr_dout),
    .sdr_rd(sdr_rd),
    .sdr_wr(sdr_wr),
    .sdr_get(sdr_get),
    .sdr_put(sdr_put)
  );
  
  SDRAM controller (
    .clk_in(sdrclk),      // cache side
    .rst(~rst),
    .ad(sdr_addr),
    .dout(sdr_din),
    .din(sdr_dout),
    .rd(sdr_rd),
    .wr(sdr_wr),
    .get(sdr_get),
    .put(sdr_put),
    
    .sd_data(sdram_d),    // SDRAM chip side
    .sd_addr(sdram_a),
    .sd_dqm(sdram_dqm),
    .sd_ba(sdram_ba),
    .sd_cs(sdram_csn),
    .sd_we(sdram_wen),
    .sd_ras(sdram_rasn),
    .sd_cas(sdram_casn),
    .sd_cke(sdram_cke),
    .sd_clk(sdram_clk)
  );

  // Video circuit: XGA over DVID
  //
  wire hs, vs, rgb, vde;
  wire [7:0] vid = {8{rgb}};

  VIDEO video (
    .clk(clk),
    .pclk(pixclk),
    .inv(1'b0),
    .cpudata(outbus),
    .cpuadr(adr),
    .wmask(wmask),
    
    .hsync(hs),
    .vsync(vs),
    .rgb(rgb),
    .vde(vde)
  );

  DVID tdms (
    .pixclk(pixclk),
    .pixclk_x5(pix5clk),
    
    .red(vid), .green(vid), .blue(vid),
    .vde(vde), .hSync(hs), .vSync(vs),
    
    .gpdi_dp(gpdi_dp)
  );

endmodule
