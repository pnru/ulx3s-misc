#!/bin/sh

yosys -p "synth_ecp5 -json sys.json" RISC5.v Divider.v FPAdder.v FPDivider.v FPMultiplier.v \
                                     MouseM.v Multiplier.v PROM.v PS2.v RS232R.v RS232T.v SPI.v \
                                     Ulx3s_Top.v cache.v sdram.v video.v dvid.v pll1.v pll2.v

nextpnr-ecp5 --85k --package CABGA381 --json sys.json --lpf ulx3s_v20.lpf --textcfg sys.cfg

ecppack sys.cfg sys.bit --compress

cp sys.bit bit/obst_85F.bit

