//
// sdram.v
//
// This source code is public domain
//
// Oberon SDRAM controller, do 256 byte bursts
// Designed to run at ~100 MHz (i.e. will not work @>110Mhz with grade 7 sdram)
//

module SDRAM (
  input             clk_in,     // controller clock
  
  // interface to the chip
  inout      [15:0] sd_data,    // 16 bit databus
  output reg [12:0] sd_addr,    // 12 bit multiplexed address bus
  output reg [1:0]  sd_dqm,     // two byte masks
  output reg [1:0]  sd_ba,      // two banks
  output            sd_cs,      // chip select
  output            sd_we,      // write enable
  output            sd_ras,     // row address select
  output            sd_cas,     // column address select
  output            sd_cke,     // clock enable
  output            sd_clk,     // chip clock (inverted from input clk)

  // Oberon cache interface
  input      [15:0] din,        // data input from cpu
  output reg [15:0] dout,       // data output to cpu
  input      [11:0] ad,         // 12 bit upper address
  output reg        get,        // load word from SDR on sdr_clk2
  output reg        put,        // send word to SDR on sdr_clk2
  input  wire       rd,         // SDR start read transaction
  input  wire       wr,         // SDR start write transaction
  input             rst         // cpu reset
);

  localparam BURST_LENGTH   = 3'b111; // 000=1, 001=2, 010=4, 011=8, 111=full page
  localparam ACCESS_TYPE    = 1'b0;   // 0=sequential, 1=interleaved
  localparam CAS_LATENCY    = 3'd3;   // 3 needed @ >100Mhz
  localparam OP_MODE        = 2'b00;  // only 00 (standard operation) allowed
  localparam WRITE_BURST    = 1'b0;   // 0=write burst enabled, 1=only single access write

  localparam MODE = {3'b000, WRITE_BURST, OP_MODE, CAS_LATENCY, ACCESS_TYPE, BURST_LENGTH};
  
  // Extend the address bus
  wire [23:0] addr;
  assign addr = { 5'h0, ad, 7'h0 };


  // ---------------------------------------------------------------------
  // --------------------------- startup/reset ---------------------------
  // ---------------------------------------------------------------------

  // make sure rst lasts long enough (recommended 100us)
  reg [4:0] reset;
  always @(posedge clk_in) begin
    reset <= (|reset) ? reset - 5'd1 : 0;
    if(rst)	reset <= 5'd25;
  end

  // ---------------------------------------------------------------------
  // ------------------ generate ram control signals ---------------------
  // ---------------------------------------------------------------------

  // all possible commands
  localparam CMD_INHIBIT         = 4'b1111;
  localparam CMD_NOP             = 4'b0111;
  localparam CMD_ACTIVE          = 4'b0011;
  localparam CMD_READ            = 4'b0101;
  localparam CMD_WRITE           = 4'b0100;
  localparam CMD_BURST_TERMINATE = 4'b0110;
  localparam CMD_PRECHARGE       = 4'b0010;
  localparam CMD_AUTO_REFRESH    = 4'b0001;
  localparam CMD_LOAD_MODE       = 4'b0000;

  assign sd_clk = !clk_in; // chip clock shifted 180 deg.
  assign sd_cke = 1'b1;

  // drive control signals according to current command
  reg  [3:0] sd_cmd; 
  assign sd_cs  = sd_cmd[3];
  assign sd_ras = sd_cmd[2];
  assign sd_cas = sd_cmd[1];
  assign sd_we  = sd_cmd[0];

  // sdram tri-state databus interaction
  reg  sd_rden, sd_wren;

`ifdef __ICARUS__
  reg   [15:0] i;
  assign sd_data  = sd_wren ? i : 16'hzzzz;
  always @(posedge sd_clk) i <= din;
  always @(posedge sd_clk) if(sd_rden) dout <= sd_data;
`else
  wire  [15:0] i, o;
  BB           bb[15:0] (.T(~sd_wren), .I(i), .O(o), .B(sd_data));
  OFS1P3BX dbo_FF[15:0] (.SCLK(sd_clk), .SP(1'b1),    .Q(i),  .D(din), .PD(1'b0));
  IFS1P3BX dbi_FF[15:0] (.SCLK(sd_clk), .SP(sd_rden), .Q(dout), .D(o), .PD(1'b0));
`endif
  
  // ---------------------------------------------------------------------
  // ------------------------ cycle state machine ------------------------
  // ---------------------------------------------------------------------
  
  // The state machine runs at 125Mhz, asynchronous to the CPU.
  // It idles doing refreshes and switches to burst reads and writes
  // as requested.
  
  localparam STBY  =   0;   // start state, do refreshes
  localparam RD    =  50;   // start of read sequence
  localparam WR    = 200;   // start of write sequence

  reg [8:0] t = 0;

  wire mreq =  rd | wr;

  always @(posedge clk_in) begin
    sd_cmd <= CMD_NOP;  // default command
    
    // move to next state
    t <= t + 1;

    if(reset != 0) begin // reset operation
      case(reset)
      22: begin sd_ba   <= 2'b00; sd_dqm  <= 2'b00;
                get     <= 1'b0;  put     <= 1'b0;
                sd_rden <= 1'b0;  sd_rden <= 1'b0;
          end
      21: sd_addr[10] <= 1'b1; // prep for precharge all banks
      20: sd_cmd  <= CMD_PRECHARGE;
      11: sd_addr <= MODE;
      10: sd_cmd  <= CMD_LOAD_MODE;
       1: t <= 0;
      endcase
    end
    
    else begin // normal operation
      case(t)

      // Idle doing refreshes
      //
      STBY:  begin
                sd_addr <= { 2'b00, addr[19:9] };
                sd_ba   <= addr[21:20];
                if (mreq) begin
                  sd_cmd <= CMD_ACTIVE;
                  t <= rd ? RD : WR;
                end
                else begin
                  sd_cmd <= CMD_AUTO_REFRESH;
                end
              end
      STBY+9: t <= STBY;
      
      // Read burst, set-up for tRCD = 2 clocks and CL = 3 clocks. Possibly the burst
      // terminate could come earlier. The 'get' signal accounts for the dbi_FF delay.
      //
      RD+2:   sd_addr <= { 4'b0010, addr[8:0] }; 
      RD+3:   sd_cmd  <= CMD_READ;               
      RD+6:   sd_rden <= 1'b1;                   
      RD+7:   get     <= 1'b1;                   
      // ... 128 read cycles
      RD+134: sd_rden <= 1'b0;                   
      RD+135: get     <= 1'b0;                   
      RD+136: sd_cmd  <= CMD_BURST_TERMINATE;    
      RD+137: sd_cmd  <= CMD_PRECHARGE; /* ALL */
      RD+140: t       <= STBY;
      
      // Write burst, set up for tRCD = 2. Burst terminate has to be exact here, and
      // the 'put' signal accounts for a 3 clock pipeline to fetch data.
      //
      WR+2:   put     <= 1'b1;                   
      WR+3:   sd_addr <= { 4'b0010, addr[8:0] }; 
      WR+4:   sd_wren <= 1'b1;
      WR+5:   sd_cmd  <= CMD_WRITE;              
      // ... 128 write cycles
      WR+131: put     <= 1'b0;                   
      WR+133: sd_cmd  <= CMD_BURST_TERMINATE;    
      WR+134: sd_wren <= 1'b0;                   
      WR+135: sd_cmd  <= CMD_PRECHARGE; /* ALL */
      WR+140: t       <= STBY;

      endcase
    end
  end

endmodule
