; A tiny monitor program for the Oberon Station
;
; This source code is public domain
;
	iobase = -64
	led    = 4
	SioDt  = 8
	SioSt  = 12
	TxRdy  = 2
	RxRdy  = 1

start:	mov	r14,iobase	; set IOBASE and set leds
	mov	r0,0xaa
	st	r0,led(r14)
	
	mov	r10,0x4100
	ld	r0,0(r10)
	
	mov	r1,0		; baud = 19K2
	st	r1,SioSt(r14)
	
	mov	r0,0		; send NUL NUL to sync
	bl	send
	mov	r0,0x01
	st	r0,led(r14)
	bl	send
	br	repl

	; output increasing numbers in hex
	;
	mov	r10,0x3f00	; start at 0x3f00
loop:	mov	r0,r10		; send address
	bl	hex8
	mov	r0,0x20		; send space
	bl	send
	ld	r0,0(r10)	; send content
	bl	hex8
	mov	r0,0x0a		; send \n
	bl	send
	add	r10,r10,4	; next number
	br	loop

	; send char in r0 to UART
	;
send:	ld	r13,SioSt(r14)	; wait Tx ready
	and	r13,r13,TxRdy
	beq	send
	st	r0,SioDt(r14)	; send char
	rt

	; receive char from UART to r0
	;
recv:	ld	r13,SioSt(r14)	; wait Rx ready
	and	r13,r13,RxRdy
	beq	recv
	ld	r0,SioDt(r14)	; recv char
	rt

	; send word in r0 in hex
	;
hex8:	mov	r3,r15		; save linkage
	mov	r4,8		; 8 digits
hx01:	ror	r0,r0,28	; next digit
	bl	hex1
	add	r4,r4,-1
	bne	hx01
	br	(r3)		; return

	; send one hex digit
	;
hex1:	mov	r2,r15		; save linkage
	mov	r1,r0		; save r0
	and	r0,r1,0xf	; mask lowest digit in r0
	sub	r13,r0,0xa	; decimal?
	bmi	hex1a		; -> yes
	add	r0,r0,0x27	; add 'a' - '0' - 10
hex1a:	add	r0,r0,0x30	; add '0'
	bl	send		; send ascii digit
	mov	r0,r1		; restore r0
	br	(r2)		; return

	; get hex number, return value in r11
	; next char in r0
	;
gethex:	mov	r12,r15		; save return
	mov	r11,0		; clear tmp
nxtdig:	bl	recv		; get digit & echo
	bl	send
	sub	r13,r0,0x30	; < '0' ?
	bmi	done
	sub	r13,r0,0x3a	; > '9' ?
	bmi	getdg
	sub	r13,r0,0x61	; < 'a' ?
	bmi	done
	sub	r13,r0,0x67	; > 'f' ?
	bpl	done
	sub	r0,r0,0x27	; 'a' - '0' - 10
getdg:	sub	r0,r0,0x30
	lsr	r11,r11,4
	add	r11,r11,r0
	br	nxtdig
done:	br	(r12)

repl:	mov	r0,0x3e		; prompt
	bl	send
	mov	r0,0x20
	bl	send
	bl	recv		; get command letter
	bl	send		; echo
	sub	r13,r0,0x64	; command d? dxxxx,yyyy = dump from xxxx to yyyy
	beq	dod
	sub	r13,r0,0x66	; command f? fxxxx,yyyy = fill from xxxx to yyyy with adr + 0x8000
	beq	dof
	sub	r13,r0,0x70	; command p? pxxxx.yyyy = put yyyy in location xxxxx
	beq	dop
	sub	r13,r0,0x76	; command v? vxxxx,yyyy = verify from xxxx to yyyy for adr + 0x8000
	beq	dov
	mov	r0,0x0a		; send nl ?
	bl	send
	mov	r0,0x3f
	bl	send
next:	mov	r0,0x0a		; send nl & prompt
	bl	send
	br	repl

dod:	bl	gethex
	mov	r10,r11
	bl	gethex
	mov	r0,0x0a		; send nl
	bl	send
nxtlin:	mov	r0,r10
	bl	hex8
	mov	r0,0x20		; send space
	bl	send
	ld	r0,0(r10)	; send content
	bl	hex8
	mov	r0,0x0a		; send \n
	bl	send
	add	r10,r10,4	; next number
	sub	r13,r11,r10	; past end?
	bpl	nxtlin
	br	next

dof:	bl	gethex
	mov	r10,r11
	bl	gethex
	mov	r0,0x0a		; send nl
	bl	send
nxtfil:	mov	r0,r10
	;add	r0,r0,0x8000
	st	r0,0(r10)
	add	r10,r10,4	; next number
	sub	r13,r11,r10	; past end?
	bpl	nxtfil
br	next

dop:	bl	gethex
	mov	r10,r11
	bl	gethex
	mov	r0,0x0a		; send nl
	bl	send
	st	r11,0(r10)
br	next

dov:	bl	gethex		; read start,end
	mov	r10,r11
	bl	gethex
	mov	r0,0x0a		; send nl
	bl	send
nxtchk:	ld	r13,0(r10)	; fetch word & check
	;sub	r13,r13,0x8000
	sub	r9,r13,r10
	beq	ok		; equal? => check next address
	mov	r0,r10		; print address
	bl	hex8
	mov	r0,0x20		; send space
	bl	send
	mov	r0,r9		; print diff
	bl	hex8
	mov	r0,0x0a		; send nl
	bl	send
	;br	next		; don't check further
ok:	add	r10,r10,4	; next address
	sub	r13,r11,r10	; past end?
	bpl	nxtchk
	br	next

