This directory contains an experimental build of the FPGA Oberon system:
http://www.projectoberon.com

It has the static ram setup replaced by a cache and sdram. This necessitated some modifications to the
RISC5 CPU, viz. adding a clock enable signal in order to pause the CPU duing a cache miss.

Also the video code has been adapted and this now uses 96KB of block RAM. This is shadow ram, i.e. the
CPU reads and writes to main memory and the block video ram tracks all writes.

As it stands, it is for ULX3S boards with an 85F FPGA chip, due to the amount of block ram required.
I'm contemplating making a version where the prom contents are handled by pre-loading the cache ram
with this code -- with that modification it should fit on boards with a 12F chip. An alternative is
to load a video scan line at a time form sdram - it is the better solution, but requires more code.

For a reason I do not currently understand, the circuit needs a long reset after loading. If the
reset is not successful, reload the bit file whilst holding btn0 (manual reset) pressed and only
release after the bit file load completes. This should not normally be necessary, but is worth trying
when there are boot problems.

Overall the circuit meets timing contraints comfortably, with the exception of the 325MHz DVI clock.
This is not normally a problem, as the monitor will compensate phase differences. Occasionally, the
build will fail the 100MHz SDRAM clock contraint (usually Fmax comes out at 115-120MHz); in these cases
using a different seed should solve the problem.

For discussion visit:
https://gitter.im/ulx3s/Lobby

Enjoy!
